///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description:
//
// Bit pipelined
// Author: David Caruso
//
////////////////////////////////////////////////////////////////////////////////

module bit_pipelined #(
    parameter STAGES = 1
)(
    input                                     CLK,
    input                                     nRST,
    input                                     DATA_IN,
    output                                    DATA_OUT
);

reg [STAGES-1:0]            data_r;

integer idx;
always @(posedge CLK) begin
    if (~nRST) begin
        for (idx = 0; idx < STAGES; idx = idx + 1) begin
            data_r[idx] <= 'd0;
        end
    end
    else begin
        data_r <= {data_r[STAGES-1:0], DATA_IN};
    end
end

assign DATA_OUT       = data_r[STAGES-1];

endmodule