/////////////////////////////////////////////////////////////////////
////                                                             ////
////  Non-restoring signed by unsigned divider                   ////
////  Uses the non-restoring unsigned by unsigned divider        ////
////                                                             ////
////  Author: Richard Herveille                                  ////
////          richard@asics.ws                                   ////
////          www.asics.ws                                       ////
////                                                             ////
/////////////////////////////////////////////////////////////////////
////                                                             ////
//// Copyright (C) 2002 Richard Herveille                        ////
////                    richard@asics.ws                         ////
////                                                             ////
//// This source file may be used and distributed without        ////
//// restriction provided that this copyright statement is not   ////
//// removed from the file and that any derivative work contains ////
//// the original copyright notice and the associated disclaimer.////
////                                                             ////
////     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ////
//// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ////
//// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ////
//// FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ////
//// OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ////
//// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ////
//// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ////
//// GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ////
//// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ////
//// LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ////
//// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ////
//// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ////
//// POSSIBILITY OF SUCH DAMAGE.                                 ////
////                                                             ////
/////////////////////////////////////////////////////////////////////
//  CVS Log
//
//  $Id: div_su.v,v 1.2 2002-10-31 13:54:58 rherveille Exp $
//
//  $Date: 2002-10-31 13:54:58 $
//  $Revision: 1.2 $
//  $Author: rherveille $
//  $Locker:  $
//  $State: Exp $
//
// Change History:
//               $Log: not supported by cvs2svn $
//               Revision 1.1.1.1  2002/10/29 20:29:09  rherveille
//
//
//
// Modified by David Caruso
//
//////////////////////////////////////////////////////////////////////


`timescale 1ns/1ps

module div_su #
(
    parameter   DIVIDEND_WIDTH = 16,
    parameter   DIVISOR_WIDTH = DIVIDEND_WIDTH/2
)
(   
    input                            CLK,
    input                            nRST,
    input                            INPUT_VALID,
    input [DIVIDEND_WIDTH-1:0]       DIVIDEND,
    input [DIVISOR_WIDTH-1:0]        DIVISOR,
    output reg [DIVISOR_WIDTH-1:0]   QUOTIENT,
    output reg [DIVISOR_WIDTH-1:0]   REMAINDER,
    output reg                       DIV_BY_ZERO,
    output reg                       OVERFLOW,
    output reg                       OUTPUT_VALID
);

    reg [DIVIDEND_WIDTH-1:0] iz;
    reg [DIVISOR_WIDTH -1:0] id;
    reg [DIVISOR_WIDTH +1:0] spipe;

    wire [DIVISOR_WIDTH-1:0] iq, is;
    wire                idiv0, iovf;
    wire                output_valid;
    wire                mantain_pipeline;
    reg [DIVISOR_WIDTH:0]  out_pipe;

    always @(posedge CLK) begin
        if (~nRST) begin
            out_pipe <= 'd0;
        end
        else begin
            out_pipe <= {out_pipe[DIVISOR_WIDTH-1:0], INPUT_VALID};
        end
    end

    assign mantain_pipeline =|out_pipe;

    // delay d
    always @(posedge CLK) begin
        if (~nRST) begin
            id <= 'd0;
        end
        else if (INPUT_VALID) begin
            id <=  DIVISOR;
        end
    end

    // check z, take abs value
    always @(posedge CLK) begin
        if (~nRST) begin
            iz <= 'd0;
        end
        else if (INPUT_VALID) begin
            if (DIVIDEND[DIVIDEND_WIDTH-1]) begin
                iz <=  ~DIVIDEND +1'h1;
            end
            else begin
                iz <=  DIVIDEND;
            end
        end
    end

    // generate spipe (sign bit pipe)
    integer n;
    always @(posedge CLK) begin
        if (~nRST) begin
            spipe <= 'd0;
        end
        else if(INPUT_VALID | mantain_pipeline) begin
            spipe[0] <=  DIVIDEND[DIVIDEND_WIDTH-1];
            for(n=1; n <= DIVISOR_WIDTH+1; n=n+1) begin
                spipe[n] <=  spipe[n-1];
            end
        end
    end

    // hookup non-restoring divider
    div_uu #(
        .DIVIDEND_WIDTH (DIVIDEND_WIDTH),
        .DIVISOR_WIDTH  (DIVISOR_WIDTH)
    )
    divider (
        .CLK            (CLK),
        .nRST           (nRST),
        .INPUT_VALID    (INPUT_VALID),
        .DIVIDEND       (iz),
        .DIVISOR        (id),
        .QUOTIENT       (iq),
        .REMAINDER      (is),
        .DIV_BY_ZERO    (idiv0),
        .OVERFLOW       (iovf),
        .OUTPUT_VALID   (output_valid)
    );

    // correct divider results if 'd' was negative
    always @(posedge CLK) begin
        if (~nRST) begin
            QUOTIENT <= 'd0;
            REMAINDER <= 'd0;
        end
        else if(output_valid) begin
            if(spipe[DIVISOR_WIDTH+1]) begin
                QUOTIENT <=  (~iq) + 1'h1;
                REMAINDER <=  (~is) + 1'h1;
            end
            else begin
                QUOTIENT <=  {1'b0, iq};
                REMAINDER <=  {1'b0, is};
            end
        end
    end

    // delay flags same as results
    always @(posedge CLK) begin
        if (~nRST) begin
            DIV_BY_ZERO <= 1'b0;
            OVERFLOW <= 1'b0;
        end
        else if(output_valid) begin
            DIV_BY_ZERO <=  idiv0;
            OVERFLOW  <=  iovf;
        end
    end

    always @(posedge CLK) begin
        if (~nRST) begin
            OUTPUT_VALID <= 1'b0;
        end
        else begin
            OUTPUT_VALID <= output_valid;
        end
    end

   `ifdef WAVEFORM_DIV
    initial begin
        $dumpfile("./sim_build/waveform.vcd");
        $dumpvars (0, div_su);
    end
  `endif

endmodule