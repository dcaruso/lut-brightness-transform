///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description: 
//
// LUT brightness transform
// Author: David Caruso
//
////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps

module lut_brightness_transform #(
    parameter STREAM_INPUT_WIDTH = 32,
    parameter STREAM_OUTPUT_WIDTH = 8,
    parameter LUT_LEN = 256
)(
    input                                    CLK,
    input                                    nRST,
    input         [STREAM_INPUT_WIDTH-1:0]   DATA_STREAM_INPUT,
    input                                    DATA_IN_VALID,
    output        [STREAM_OUTPUT_WIDTH-1:0]  DATA_STREAM_OUTPUT,
    output                                   DATA_OUT_VALID,
    // Input Stream LUT charge port
    input         [STREAM_INPUT_WIDTH-1:0]   DATA_LUT_IN,
    input                                    DATA_LUT_IN_VALID,
    input         [$clog2(LUT_LEN)-1:0]      ADDR_LUT_IN,
    // Ouput Stream LUT charge port
    input         [STREAM_OUTPUT_WIDTH-1:0]  DATA_LUT_OUT,
    input                                    DATA_LUT_OUT_VALID,
    input         [$clog2(LUT_LEN)-1:0]      ADDR_LUT_OUT,
    input                                    LOAD_TABLE
);

localparam STAGES_QUEST   = $clog2(LUT_LEN)+1;
localparam LUT_ADDR_WIDTH = $clog2(LUT_LEN);
wire [LUT_ADDR_WIDTH-1:0]                  addr_lut[0:STAGES_QUEST-1];
wire [STREAM_INPUT_WIDTH-1:0]              data_lut[0:STAGES_QUEST-1];
wire [STREAM_INPUT_WIDTH-1:0]              data_lut_0[0:STAGES_QUEST-1];
wire [STREAM_INPUT_WIDTH-1:0]              data_lut_1[0:STAGES_QUEST-1];
wire [LUT_ADDR_WIDTH*STAGES_QUEST-1:0]     addr_lut_pack;
wire [STREAM_INPUT_WIDTH*STAGES_QUEST-1:0] data_lut_pack;

wire signed [STREAM_INPUT_WIDTH-1:0]       xi_value_found, xa_value_found, xb_value_found;
reg signed [STREAM_INPUT_WIDTH-1:0]        xi_value_c, xa_value_c, xb_value_c;
wire [LUT_ADDR_WIDTH-1:0]                  xi_addr_found, xa_addr_found, xb_addr_found;
wire                                       found;
reg                                        found_r;

wire [STREAM_OUTPUT_WIDTH-1:0]             ya_value_found, yb_value_found;
wire [STREAM_OUTPUT_WIDTH-1:0]             ya_value_found_m [0:1];
wire [STREAM_OUTPUT_WIDTH-1:0]             yb_value_found_m [0:1];
reg  [STREAM_OUTPUT_WIDTH-1:0]             ya_value_c, yb_value_c;

wire [0:1]                                 data_lut_in_valid, data_lut_out_valid;


genvar unpack_addr_idx;
generate
  for (unpack_addr_idx=0; unpack_addr_idx<(STAGES_QUEST); unpack_addr_idx=unpack_addr_idx+1) begin
    assign addr_lut[unpack_addr_idx][((LUT_ADDR_WIDTH)-1):0] = addr_lut_pack[(LUT_ADDR_WIDTH)*unpack_addr_idx +: LUT_ADDR_WIDTH];
  end
endgenerate

genvar pack_data_idx;

generate
  for (pack_data_idx=0; pack_data_idx<(STAGES_QUEST); pack_data_idx=pack_data_idx+1) begin
    assign data_lut_pack[(STREAM_INPUT_WIDTH)*pack_data_idx +: STREAM_INPUT_WIDTH] = data_lut[pack_data_idx][((STREAM_INPUT_WIDTH)-1):0];
  end
endgenerate

genvar i,j;
generate
    for (i=0; i<STAGES_QUEST; i=i+1) begin: gen_lut_for_stages
        dual_port_ram #(
            .DATA_WIDTH         (STREAM_INPUT_WIDTH),
            .MEMORY_SIZE        (LUT_LEN)
        )ram_input_stream_0(
            .CLKA               (CLK),
            .CLKB               (CLK),
            .WEA                (data_lut_in_valid[0]),
            .ADDRA              (ADDR_LUT_IN),
            .ADDRB              (addr_lut[i]),
            .DATA_A             (DATA_LUT_IN),
            .DATA_B             (data_lut_0[i])
        );

        dual_port_ram #(
            .DATA_WIDTH         (STREAM_INPUT_WIDTH),
            .MEMORY_SIZE        (LUT_LEN)
        )ram_input_stream_1(
            .CLKA               (CLK),
            .CLKB               (CLK),
            .WEA                (data_lut_in_valid[1]),
            .ADDRA              (ADDR_LUT_IN),
            .ADDRB              (addr_lut[i]),
            .DATA_A             (DATA_LUT_IN),
            .DATA_B             (data_lut_1[i])
        );

        assign data_lut[i] = (LOAD_TABLE)? data_lut_0[i]:data_lut_1[i];
    end

    for (i =0; i<2; i= i+1) begin: gen_lut_out_multiple
        dual_port_ram #(
            .DATA_WIDTH         (STREAM_OUTPUT_WIDTH),
            .MEMORY_SIZE        (LUT_LEN)
        )ram_output_streamA(
            .CLKA               (CLK),
            .CLKB               (CLK),
            .WEA                (data_lut_out_valid[i]),
            .ADDRA              (ADDR_LUT_OUT),
            .ADDRB              (xa_addr_found),
            .DATA_A             (DATA_LUT_OUT),
            .DATA_B             (ya_value_found_m[i])
        );

        dual_port_ram #(
            .DATA_WIDTH         (STREAM_OUTPUT_WIDTH),
            .MEMORY_SIZE        (LUT_LEN)
        )ram_output_streamB(
            .CLKA               (CLK),
            .CLKB               (CLK),
            .WEA                (data_lut_out_valid[i]),
            .ADDRA              (ADDR_LUT_OUT),
            .ADDRB              (xb_addr_found),
            .DATA_A             (DATA_LUT_OUT),
            .DATA_B             (yb_value_found_m[i])
        );
    end

endgenerate

assign data_lut_out_valid[0] = (LOAD_TABLE)? 1'b0:DATA_LUT_OUT_VALID;
assign data_lut_out_valid[1] = (LOAD_TABLE)? DATA_LUT_OUT_VALID:1'b0;
assign data_lut_in_valid[0]  = (LOAD_TABLE)? 1'b0:DATA_LUT_IN_VALID;
assign data_lut_in_valid[1]  = (LOAD_TABLE)? DATA_LUT_IN_VALID:1'b0;

assign ya_value_found = (LOAD_TABLE)? ya_value_found_m[0]:ya_value_found_m[1];
assign yb_value_found = (LOAD_TABLE)? yb_value_found_m[0]:yb_value_found_m[1];
    lut_seeker #(
        .X_WIDTH            (STREAM_INPUT_WIDTH),
        .LUT_LEN            (LUT_LEN),
        .STAGES_QUEST       (STAGES_QUEST)
    )lut_seeker_device(
        .CLK                (CLK),
        .nRST               (nRST),
        .XI_VALID           (DATA_IN_VALID),
        .XI_VALUE           (DATA_STREAM_INPUT),
        .XI_VALUE_FOUND     (xi_value_found),
        .FOUND              (found),
        .XA_VALUE_FOUND     (xa_value_found),
        .XA_ADDR_FOUND      (xa_addr_found),
        .XB_VALUE_FOUND     (xb_value_found),
        .XB_ADDR_FOUND      (xb_addr_found),
        .ADDR_LUTS          (addr_lut_pack),
        .DATA_LUTS          (data_lut_pack)
    );

// Outlayers corrector
    always @(posedge CLK) begin
        if (~nRST) begin
            found_r <= 1'b0;
            xi_value_c <= 'd0;
            xa_value_c <= 'd0;
            xb_value_c <= 'd0;
            ya_value_c <= 'd0;
            yb_value_c <= 'd0;
        end
        else begin
            found_r <= found;
            if (found) begin
                xi_value_c <= xi_value_found;
                xb_value_c <= xb_value_found;
                xa_value_c <= xa_value_found;
                ya_value_c <= ya_value_found;
                yb_value_c <= yb_value_found;
                if ($signed(xi_value_found) < $signed(xa_value_found)) begin
                    xi_value_c <= xa_value_found;
                end
                else begin
                    if ($signed(xi_value_found) > $signed(xb_value_found)) begin
                        xi_value_c <= xb_value_found;
                    end
                end
            end
        end
    end

    interpolator #(
        .Y_WIDTH            (STREAM_OUTPUT_WIDTH),
        .X_WIDTH            (STREAM_INPUT_WIDTH)
    )interpolator_device(
        .CLK                (CLK),
        .nRST               (nRST),
        .XI                 (xi_value_c),
        .XA                 (xa_value_c),
        .XB                 (xb_value_c),
        .YA                 (ya_value_c),
        .YB                 (yb_value_c),
        .DO_INTERPOLATION   (found_r),
        .OUTPUT_VALID       (DATA_OUT_VALID),
        .YO                 (DATA_STREAM_OUTPUT)
    );

`ifdef WAVEFORM_LUT_BRIGHTNESS_TRANSFORM
    initial begin
        $dumpfile("./sim_build/waveform.vcd");
        $dumpvars (0, lut_brightness_transform);
    end
`endif

endmodule