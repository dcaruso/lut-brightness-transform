///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description: 
//
// Simple Dual Port RAM with asyncrhonous read process, generate distributed RAM
// Author: David Caruso
//
////////////////////////////////////////////////////////////////////////////////

module dual_port_ram #(
    parameter DATA_WIDTH  = 32,
    parameter MEMORY_SIZE = 256
)(
    input                            CLKA,
    input                            CLKB,
    input                            WEA,
    input  [$clog2(MEMORY_SIZE)-1:0] ADDRA,
    input  [$clog2(MEMORY_SIZE)-1:0] ADDRB,
    input  [(DATA_WIDTH-1):0]        DATA_A,
    output [(DATA_WIDTH-1):0]        DATA_B
);

reg [(DATA_WIDTH-1):0] ram [(MEMORY_SIZE-1):0];

always @(posedge CLKA) begin
    if (WEA)
        ram[ADDRA] <= DATA_A;
end

assign DATA_B = ram[ADDRB];

endmodule