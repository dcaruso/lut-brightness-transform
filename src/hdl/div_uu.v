/////////////////////////////////////////////////////////////////////
////                                                             ////
////  Non-restoring unsigned divider                             ////
////                                                             ////
////  Author: Richard Herveille                                  ////
////          richard@asics.ws                                   ////
////          www.asics.ws                                       ////
////                                                             ////
/////////////////////////////////////////////////////////////////////
////                                                             ////
//// Copyright (C) 2002 Richard Herveille                        ////
////                    richard@asics.ws                         ////
////                                                             ////
//// This source file may be used and distributed without        ////
//// restriction provided that this copyright statement is not   ////
//// removed from the file and that any derivative work contains ////
//// the original copyright notice and the associated disclaimer.////
////                                                             ////
////     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ////
//// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ////
//// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ////
//// FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ////
//// OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ////
//// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ////
//// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ////
//// GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ////
//// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ////
//// LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ////
//// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ////
//// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ////
//// POSSIBILITY OF SUCH DAMAGE.                                 ////
////                                                             ////
/////////////////////////////////////////////////////////////////////

//  CVS Log
//
//  $Id: div_uu.v,v 1.3 2003-09-17 13:08:53 rherveille Exp $
//
//  $Date: 2003-09-17 13:08:53 $
//  $Revision: 1.3 $
//  $Author: rherveille $
//  $Locker:  $
//  $State: Exp $
//
// Change History:
//               $Log: not supported by cvs2svn $
//               Revision 1.2  2002/10/31 13:54:58  rherveille
//               Fixed a bug in the remainder output of div_su.v
//
//               Revision 1.1.1.1  2002/10/29 20:29:10  rherveille
//
//
//       
// Modified by David Caruso
//
//////////////////////////////////////////////////////////////////////

module div_uu #
(
    parameter   DIVIDEND_WIDTH = 32,
    parameter   DIVISOR_WIDTH = DIVIDEND_WIDTH/2
)
(
    input                           CLK,
    input                           nRST,
    input                           INPUT_VALID,
    input [DIVIDEND_WIDTH-1:0]      DIVIDEND,
    input [DIVISOR_WIDTH-1:0]       DIVISOR,
    output reg [DIVISOR_WIDTH-1:0]  QUOTIENT,
    output reg [DIVISOR_WIDTH-1:0]  REMAINDER,
    output reg                      DIV_BY_ZERO,
    output reg                      OVERFLOW,
    output reg                      OUTPUT_VALID
);

    function [DIVIDEND_WIDTH:0] gen_s;
        input [DIVIDEND_WIDTH:0] si;
        input [DIVIDEND_WIDTH:0] di;
    begin
        if(si[DIVIDEND_WIDTH])
            gen_s = {si[DIVIDEND_WIDTH-1:0], 1'b0} + di;
        else
            gen_s = {si[DIVIDEND_WIDTH-1:0], 1'b0} - di;
    end
    endfunction

    function [DIVISOR_WIDTH-1:0] gen_q;
        input [DIVISOR_WIDTH-1:0] qi;
        input [DIVIDEND_WIDTH:0] si;
    begin
        gen_q = {qi[DIVISOR_WIDTH-2:0], ~si[DIVIDEND_WIDTH]};
    end
    endfunction

    function [DIVISOR_WIDTH-1:0] assign_s;
        input [DIVIDEND_WIDTH:0] si;
        input [DIVIDEND_WIDTH:0] di;
        reg [DIVIDEND_WIDTH:0] tmp;
    begin
        if(si[DIVIDEND_WIDTH])
            tmp = si + di;
        else
            tmp = si;
        assign_s = tmp[DIVIDEND_WIDTH-1:DIVIDEND_WIDTH-DIVISOR_WIDTH];
    end
    endfunction

    reg [DIVISOR_WIDTH-1:0] q_pipe  [DIVISOR_WIDTH-1:0];
    reg [DIVISOR_WIDTH:0]  out_pipe;
    reg [DIVIDEND_WIDTH:0] s_pipe  [DIVISOR_WIDTH:0];
    reg [DIVIDEND_WIDTH:0] d_pipe  [DIVISOR_WIDTH:0];

    reg [DIVISOR_WIDTH:0] DIV_BY_ZERO_pipe, ovf_pipe;
    wire out_valid;

    wire                 mantain_pipeline;

    assign mantain_pipeline =| out_pipe;

    integer n0, n1, n2, n3;

    // generate divisor (d) pipe
    always @(DIVISOR) begin
        d_pipe[0] <= {1'b0, DIVISOR, {(DIVIDEND_WIDTH-DIVISOR_WIDTH){1'b0}} };
    end

    always @(posedge CLK) begin
        if (~nRST) begin
            for(n0=1; n0 <= DIVISOR_WIDTH; n0=n0+1) begin
                d_pipe[n0] <=  'd0;
            end
        end
        else if(INPUT_VALID | mantain_pipeline) begin
            for(n0=1; n0 <= DIVISOR_WIDTH; n0=n0+1) begin
                d_pipe[n0] <=  d_pipe[n0-1];
            end
        end
    end

    // generate internal remainder pipe
    always @(DIVIDEND) begin
        s_pipe[0] <= DIVIDEND;
    end

    always @(posedge CLK) begin
        if (~nRST) begin
            for(n1=1; n1 <= DIVISOR_WIDTH; n1=n1+1) begin
                s_pipe[n1] <= 'd0;
            end
        end
        else if(INPUT_VALID | mantain_pipeline) begin
            for(n1=1; n1 <= DIVISOR_WIDTH; n1=n1+1) begin
                s_pipe[n1] <=  gen_s(s_pipe[n1-1], d_pipe[n1-1]);
            end
        end
    end

    always @(posedge CLK) begin
        if (~nRST) begin
            for(n2=0; n2 < DIVISOR_WIDTH-1; n2=n2+1) begin
                q_pipe[n2] <= 'd0;
            end
        end
        else if(INPUT_VALID | mantain_pipeline) begin
            for(n2=1; n2 < DIVISOR_WIDTH; n2=n2+1) begin
                q_pipe[n2] <=  gen_q(q_pipe[n2-1], s_pipe[n2]);
            end
        end
    end

    // flags (divide_by_zero, overflow)
    always @(DIVIDEND or DIVISOR) begin
        ovf_pipe[0]  <= !(DIVIDEND[DIVIDEND_WIDTH-1:DIVISOR_WIDTH] < DIVISOR);
        DIV_BY_ZERO_pipe[0] <= ~|DIVISOR;
    end

    always @(posedge CLK) begin
        if (~nRST) begin
            for(n3=1; n3 <= DIVISOR_WIDTH; n3=n3+1) begin
                ovf_pipe[n3] <= 'd0;
                DIV_BY_ZERO_pipe[n3] <= 'd0;
            end
        end
        else if(INPUT_VALID | mantain_pipeline) begin
            for(n3=1; n3 <= DIVISOR_WIDTH; n3=n3+1) begin
                ovf_pipe[n3] <=  ovf_pipe[n3-1];
                DIV_BY_ZERO_pipe[n3] <=  DIV_BY_ZERO_pipe[n3-1];
            end
        end
    end

    // assign outputs
    always @(posedge CLK) begin
        if (~nRST) begin
            OVERFLOW <= 1'b0;
        end
        else if(out_valid) begin
            OVERFLOW <=  ovf_pipe[DIVISOR_WIDTH];
        end
    end

    always @(posedge CLK) begin
        if (~nRST) begin
            DIV_BY_ZERO <= 1'b0;
        end
        else if(out_valid) begin
            DIV_BY_ZERO <=  DIV_BY_ZERO_pipe[DIVISOR_WIDTH];
        end
    end

    always @(posedge CLK) begin
        if (~nRST) begin
            QUOTIENT <= 'd0;
        end
        else if(out_valid) begin
            QUOTIENT <=  gen_q(q_pipe[DIVISOR_WIDTH-1], s_pipe[DIVISOR_WIDTH]);
        end
    end

    always @(posedge CLK) begin
        if (~nRST) begin
            REMAINDER <= 'd0;
        end
        else if(out_valid) begin
            REMAINDER <=  assign_s(s_pipe[DIVISOR_WIDTH], d_pipe[DIVISOR_WIDTH]);
        end
    end

    always @(posedge CLK) begin
        if (~nRST) begin
            out_pipe <= 'd0;
        end
        else begin
            out_pipe <= {out_pipe[DIVISOR_WIDTH-1:0], INPUT_VALID};
        end
    end

    assign out_valid = out_pipe[DIVISOR_WIDTH];

    always @(posedge CLK) begin
        if (~nRST) begin
            OUTPUT_VALID <= 1'b0;
        end
        else begin
            OUTPUT_VALID <= out_valid;
        end
    end

endmodule