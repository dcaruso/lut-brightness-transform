///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description: 
//
// Interpolator core, receive to points (XA, YA),(XB, YB) and another coord (X) 
// and calculates the Y value.
// The interpolator use this equation
//
//  YO = (XI-XA)(YB-YA)/(XB-XA) + YA
//
// To do that use 4 stages:
//
// Stage 1: Difference
//      Calculate:
//          * (XI-XA) -> diff_xIA [32 bits]
//          * (YB-YA) -> diff_yBA [8 bits]
//          * (XB-XA) -> diff_xBA [32 bits]
//
// Stage 2: Multiplication
//      Calculate:
//          * diff_xIA * diff_yBA -> mult_xy [41 bits]
//
// Stage 3: Division
//      Calculate:
//          * mult_xy / diff_xBA_r -> delta_y [8 bits]
//
// Stage 4: Sum
//      Calculate:
//          * delta_y + YA_rrr -> YO [8 bits] 
//          YA_rrr means YA signal with 3 stages of delays
//
// The process start with a pulse in DO_INTERPOLATION and ends with a pulse in OUTPUT_VALID
// Author: David Caruso
//
////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps

module interpolator #(
    parameter X_WIDTH = 32,
    parameter Y_WIDTH = 8  
)(
    input                         CLK,
    input                         nRST,
    input  signed [X_WIDTH-1:0]   XI,
    input  signed [X_WIDTH-1:0]   XA,
    input  signed [X_WIDTH-1:0]   XB,
    output reg [Y_WIDTH-1:0]      YO,
    input  [Y_WIDTH-1:0]          YA,
    input  [Y_WIDTH-1:0]          YB,
    input                         DO_INTERPOLATION,
    output reg                    OUTPUT_VALID
);

reg [X_WIDTH-1:0]                   diff_xIA;
reg [X_WIDTH-1:0]                   diff_xBA, diff_xBA_r;
wire signed [X_WIDTH:0]             diff_xIA_s;
wire signed [X_WIDTH:0]             diff_xBA_s;
wire signed [Y_WIDTH:0]             diff_yBA_s;
reg signed [Y_WIDTH:0]              diff_yBA;
reg [Y_WIDTH-1:0]                   YA_r, YA_rr;
wire [Y_WIDTH-1:0]                  YA_rrr;
reg                                 valid_r, valid_rr;
wire                                valid_rrr;
reg signed [X_WIDTH+Y_WIDTH+1:0]    mult_xy;
wire signed [X_WIDTH*2-1:0]         mult_xy_r;
wire signed [X_WIDTH-1:0]           delta_y;
wire signed [Y_WIDTH-1:0]           delta_y_t;

// Stage 1: Difference
//      Calculate:
//          * (XI-XA) -> diff_xIA [32 bits]
//          * (YB-YA) -> diff_yBA [8 bits]
//          * (XB-XA) -> diff_xBA [32 bits]
always @(posedge CLK) begin
    // Register delay signals
    valid_r <= DO_INTERPOLATION;
    YA_r <= YA;
    if (~nRST) begin
        diff_xIA <= 'd0;
        diff_yBA <= 'd0;
        diff_xBA <= 'd0;
        YA_r <= 'd0;
        valid_r <= 1'b0;        
    end
    else if (DO_INTERPOLATION) begin
        diff_xIA <= XI - XA;
        diff_xBA <= XB - XA;
        diff_yBA <= YB - YA;
    end
end

// Stage 2: Multiplication
//      Calculate:
//          * diff_xIA * diff_yBA -> mult_xy [41 bits]
assign diff_yBA_s = $signed(diff_yBA);
assign diff_xIA_s ={1'b0,diff_xIA};
always @(posedge CLK) begin
    // Register delay signals
    diff_xBA_r <= diff_xBA;
    valid_rr <= valid_r;
    YA_rr <= YA_r;
    if (~nRST) begin
        mult_xy <= 'd0;
        YA_rr <= 'd0;
        valid_rr <= 1'b0;      
        diff_xBA_r <= diff_xBA;  
    end
    else if (valid_r) begin
        mult_xy <= diff_xIA_s * diff_yBA_s;
    end
end

// Stage 3: Division
//      Calculate:
//          * mult_xy / diff_xBA_r -> delta_y [8 bits]
//
    reg_pipelined #(
        .STAGES         (X_WIDTH+3),
        .DATA_WIDTH     (Y_WIDTH)
    ) data_stage_1_delayed(
        .CLK            (CLK),
        .nRST           (nRST),
        .DATA_IN        (YA_rr),
        .DATA_OUT       (YA_rrr)
    );

    assign mult_xy_r = {{(X_WIDTH-Y_WIDTH-1){mult_xy[X_WIDTH+Y_WIDTH]}},mult_xy};

    div_su #(
    .DIVIDEND_WIDTH     (X_WIDTH*2))
    divider (
    .CLK                (CLK),
    .nRST               (nRST),
    .INPUT_VALID        (valid_rr),
    .DIVIDEND           (mult_xy_r),
    .DIVISOR            (diff_xBA_r),
    .QUOTIENT           (delta_y),
    .REMAINDER          (),
    .DIV_BY_ZERO        (div_by_zero),
    .OVERFLOW           (),
    .OUTPUT_VALID       (valid_rrr)
    );

assign delta_y_t = (div_by_zero)? 'd0:$signed(delta_y[Y_WIDTH-1:0]);

// Stage 4: Sum
//      Calculate:
//          * delta_y + YA_rrr -> YO [8 bits] 
always @(posedge CLK) begin
    OUTPUT_VALID <= valid_rrr;
    if (~nRST) begin
        YO <= 'd0;
        OUTPUT_VALID <= 1'b0;        
    end
    else if (valid_rrr) begin
        YO <= delta_y_t + YA_rrr;
    end
end

`ifdef WAVEFORM_INTERPOLATOR
    initial begin
        $dumpfile("./sim_build/waveform.vcd");
        $dumpvars (0, interpolator);
    end
`endif


endmodule