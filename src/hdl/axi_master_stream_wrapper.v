///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description: 
//
// AXI Stream Master (source) based on xilinx implementation
// Author: David Caruso
//
////////////////////////////////////////////////////////////////////////////////

module axi_master_stream_wrapper #
(
    parameter integer C_M_AXIS_TDATA_WIDTH  = 32
)
(
    // Users to add ports here
    input [C_M_AXIS_TDATA_WIDTH-1:0]            DATA_IN,
    input                                       DATA_VALID,
    output                                      DATA_READY,
    input                                       DATA_TLAST,
    // AXI Stream signals
    input                                       M_AXIS_ACLK,
    input                                       M_AXIS_ARESETN,
    output                                      M_AXIS_TVALID,
    output [C_M_AXIS_TDATA_WIDTH-1:0]           M_AXIS_TDATA,
    output [(C_M_AXIS_TDATA_WIDTH/8)-1:0]       M_AXIS_TSTRB,
    output                                      M_AXIS_TLAST,
    input                                       M_AXIS_TREADY
);

    assign M_AXIS_TDATA  = DATA_IN;
    assign M_AXIS_TVALID = DATA_VALID & M_AXIS_TREADY;
    assign DATA_READY    = M_AXIS_TREADY;
    assign M_AXIS_TLAST  = DATA_TLAST;
    assign M_AXIS_TSTRB  = {(C_M_AXIS_TDATA_WIDTH/8){1'b1}};
endmodule