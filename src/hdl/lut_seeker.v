///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description:
//
// LUT sekeer, found the X number in a list (LUT)
// Author: David Caruso
//
////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps

module lut_seeker #(
    parameter X_WIDTH = 32,
    parameter LUT_LEN = 256,
    parameter STAGES_QUEST=9
)(
    input                                     CLK,
    input                                     nRST,
    input                                     XI_VALID,
    input  signed [X_WIDTH-1:0]               XI_VALUE,
    output                                    FOUND,
    output signed [X_WIDTH-1:0]               XI_VALUE_FOUND,
    output signed [X_WIDTH-1:0]               XA_VALUE_FOUND,
    output [$clog2(LUT_LEN)-1:0]              XA_ADDR_FOUND,
    output signed [X_WIDTH-1:0]               XB_VALUE_FOUND,
    output [$clog2(LUT_LEN)-1:0]              XB_ADDR_FOUND,
    output [$clog2(LUT_LEN)*STAGES_QUEST-1:0] ADDR_LUTS,
    input [X_WIDTH*STAGES_QUEST-1:0]          DATA_LUTS
);

localparam LUT_ADDR_WIDTH = $clog2(LUT_LEN);
localparam START_ADDR = LUT_LEN/2;
localparam END_ADDR = LUT_LEN-1;
reg  [LUT_ADDR_WIDTH-1:0]        addr_lut[0:STAGES_QUEST-1];
wire [X_WIDTH-1:0]               data_lut[0:STAGES_QUEST-1];
reg  signed [X_WIDTH-1:0]        xi_r[0:STAGES_QUEST];
wire signed [X_WIDTH-1:0]        data_lut_1;
wire [LUT_ADDR_WIDTH-1:0]        addr_lut_1;
wire signed [X_WIDTH-1:0]        data_lut_2;
wire [LUT_ADDR_WIDTH-1:0]        addr_lut_2;
reg  [STAGES_QUEST:0]            valid_r;
reg  [STAGES_QUEST-1:0]          equal_r;
reg  [STAGES_QUEST-1:0]          bigger_r;
wire                             bigger_2;
wire                             out_lower;

genvar pack_addr_idx;
generate
  for (pack_addr_idx=0; pack_addr_idx<(STAGES_QUEST); pack_addr_idx=pack_addr_idx+1) begin
    assign ADDR_LUTS[(LUT_ADDR_WIDTH)*pack_addr_idx +: LUT_ADDR_WIDTH] = addr_lut[pack_addr_idx][((LUT_ADDR_WIDTH)-1):0];
  end
endgenerate

genvar unpack_data_idx;

generate
  for (unpack_data_idx=0; unpack_data_idx<(STAGES_QUEST); unpack_data_idx=unpack_data_idx+1) begin
    assign data_lut[unpack_data_idx][((X_WIDTH)-1):0] = DATA_LUTS[(X_WIDTH)*unpack_data_idx +: X_WIDTH];
  end
endgenerate

integer idx;
always @(posedge CLK) begin
    if (~nRST) begin
        valid_r <= 'd0;
        for (idx = 0; idx <= STAGES_QUEST; idx = idx + 1) begin
            xi_r[idx] <= 'd0;
        end
    end
    else begin
        valid_r <= {valid_r[STAGES_QUEST-1:0], XI_VALID};
        xi_r[0] <= XI_VALUE;
        for (idx = 1; idx <= STAGES_QUEST; idx = idx + 1) begin
            xi_r[idx] <= xi_r[idx-1];
        end
    end
end

integer i;
        // address selector, for binary search

    always @(posedge CLK) begin
        for (i=0; i<STAGES_QUEST; i=i+1) begin
            if (~nRST) begin
                bigger_r[i] <= 1'b0;
                equal_r[i]  <= 1'b0;
            end
            else if (valid_r[i]) begin
                if (xi_r[i] == $signed(data_lut[i])) begin
                    equal_r[i] <= 1'b1;
                end
                else begin
                    equal_r[i] <= 1'b0;
                    if (xi_r[i] > $signed(data_lut[i])) begin
                        bigger_r[i] <= 1'b1;
                    end
                    else begin
                        bigger_r[i] <= 1'b0;
                    end
                end
            end
        end
    end

    always @(posedge CLK) begin
        for (i=0; i<STAGES_QUEST; i=i+1) begin
            if (~nRST) begin
                addr_lut[i] <= START_ADDR;
            end
            else if (valid_r[i]) begin
                if ((i == (STAGES_QUEST-2)) && (xi_r[i] == $signed(data_lut[i]))) begin
                    addr_lut[i+1] <= addr_lut[i]; // if found the number, copy to next stage only for the last one
                end
                else begin
                    if (i < (STAGES_QUEST-1))
                        addr_lut[i+1] <= addr_lut[i] | (1<<(LUT_ADDR_WIDTH-i-2)); // half or double
                    if (xi_r[i] > $signed(data_lut[i])) begin
                        if (i < (STAGES_QUEST-1))
                            addr_lut[i+1][LUT_ADDR_WIDTH-i-1] <= 1'b1;
                        if (i == (STAGES_QUEST-2))
                            if (addr_lut[i]<END_ADDR)
                                addr_lut[i+1] <= addr_lut[i]+1;
                    end
                    else begin
                        if (i < (STAGES_QUEST-1))
                            addr_lut[i+1][LUT_ADDR_WIDTH-i-1] <= 1'b0;
                        if (i == (STAGES_QUEST-2))
                            if (addr_lut[i]>'d0)
                                addr_lut[i+1] <= addr_lut[i]-1;
                    end
                end
            end
        end
    end

// Save the STAGE-1 and STAGE-2 values for the streaming mode
    reg_pipelined #(
        .STAGES         (2),
        .DATA_WIDTH     (X_WIDTH)
    ) data_stage_2_delayed(
        .CLK            (CLK),
        .nRST           (nRST),
        .DATA_IN        (data_lut[STAGES_QUEST-2]),
        .DATA_OUT       (data_lut_2)
    );

    reg_pipelined #(
        .STAGES         (2),
        .DATA_WIDTH     ($clog2(LUT_LEN))
    ) addr_stage_2_delayed(
        .CLK            (CLK),
        .nRST           (nRST),
        .DATA_IN        (addr_lut[STAGES_QUEST-2]),
        .DATA_OUT       (addr_lut_2)
    );

    reg_pipelined #(
        .STAGES         (1),
        .DATA_WIDTH     (X_WIDTH)
    ) data_stage_1_delayed(
        .CLK            (CLK),
        .nRST           (nRST),
        .DATA_IN        (data_lut[STAGES_QUEST-1]),
        .DATA_OUT       (data_lut_1)
    );

    reg_pipelined #(
        .STAGES         (1),
        .DATA_WIDTH     ($clog2(LUT_LEN))
    ) addr_stage_1_delayed(
        .CLK            (CLK),
        .nRST           (nRST),
        .DATA_IN        (addr_lut[STAGES_QUEST-1]),
        .DATA_OUT       (addr_lut_1)
    );

    bit_pipelined bigger_stage_2_delayed(
        .CLK            (CLK),
        .nRST           (nRST),
        .DATA_IN        (bigger_r[STAGES_QUEST-2]),
        .DATA_OUT       (bigger_2)
    );

assign out_lower = (~bigger_r[STAGES_QUEST-1])&(~bigger_2);
assign out_upper = (bigger_r[STAGES_QUEST-1])&(bigger_2);

assign FOUND          = valid_r[STAGES_QUEST];
assign XI_VALUE_FOUND = xi_r[STAGES_QUEST];
assign XA_VALUE_FOUND = (equal_r[STAGES_QUEST-1]|out_lower)? data_lut_1:((bigger_r[STAGES_QUEST-1])? data_lut_1:data_lut_2);
assign XA_ADDR_FOUND  = (equal_r[STAGES_QUEST-1]|out_lower)? addr_lut_1:((bigger_r[STAGES_QUEST-1])? addr_lut_1:addr_lut_2);
assign XB_VALUE_FOUND = (equal_r[STAGES_QUEST-1]|out_lower)? data_lut_1:((bigger_r[STAGES_QUEST-1])? data_lut_2:data_lut_1);
assign XB_ADDR_FOUND  = (equal_r[STAGES_QUEST-1]|out_lower)? addr_lut_1:((bigger_r[STAGES_QUEST-1])? addr_lut_2:addr_lut_1);

endmodule
