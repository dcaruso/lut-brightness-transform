///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description: 
//
// TOP level lut brightness transform with AXI interfaces
// Author: David Caruso
//
///////////////////////////////////////////////////////////////////////////////

module axi_lut_brightness_transform #
(
    parameter LUT_LEN = 256,
    parameter STREAM_INPUT_WIDTH = 32,
    parameter STREAM_OUTPUT_WIDTH = 8,
    parameter AXI_LITE_ID = 32'hCAFECAFE
)
(   // Common AXI CLK and RESET
    input                                   ACLK,
    input                                   ARESETN,
     // AXI Slave Lite signals
    input [$clog2(LUT_LEN)+3:0]             S_AXI_AWADDR,
    input [2 : 0]                           S_AXI_AWPROT,
    input                                   S_AXI_AWVALID,
    output                                  S_AXI_AWREADY,
    input [STREAM_INPUT_WIDTH-1:0]          S_AXI_WDATA,  
    input [(STREAM_INPUT_WIDTH/8)-1:0]      S_AXI_WSTRB,
    input                                   S_AXI_WVALID,
    output                                  S_AXI_WREADY,
    output [1:0]                            S_AXI_BRESP,
    output                                  S_AXI_BVALID,
    input                                   S_AXI_BREADY,
    input [$clog2(LUT_LEN)+3:0]             S_AXI_ARADDR,
    input [2:0]                             S_AXI_ARPROT,
    input                                   S_AXI_ARVALID,
    output                                  S_AXI_ARREADY,
    output [STREAM_INPUT_WIDTH-1:0]         S_AXI_RDATA,
    output [1:0]                            S_AXI_RRESP,
    output                                  S_AXI_RVALID,
    input                                   S_AXI_RREADY,
    // AXI Slave Stream signals, input stream
    output                                  S_AXIS_TREADY,
    input  [STREAM_INPUT_WIDTH-1:0]         S_AXIS_TDATA,
    input                                   S_AXIS_TLAST,
    input                                   S_AXIS_TVALID,
    // AXI Master Stream signals output stream
    output                                  M_AXIS_TVALID,
    output [STREAM_OUTPUT_WIDTH-1:0]        M_AXIS_TDATA,
    output [(STREAM_OUTPUT_WIDTH/8)-1:0]    M_AXIS_TSTRB,
    output                                  M_AXIS_TLAST,
    input                                   M_AXIS_TREADY
);

    wire [$clog2(LUT_LEN)-1:0]              addr_lut_in, addr_lut_out;
    wire [STREAM_INPUT_WIDTH-1:0]           data_lut_in, data_lut_out2adj;
    wire [STREAM_OUTPUT_WIDTH-1:0]          data_lut_out;
    wire                                    data_lut_in_valid, data_lut_out_valid;
    wire                                    table_load, enable_device;

    wire [STREAM_INPUT_WIDTH-1:0]           data_stream_in;
    wire [STREAM_OUTPUT_WIDTH-1:0]          data_stream_out;
    wire                                    data_stream_out_ready, data_stream_out_valid, data_stream_out_tlast;
    wire                                    data_stream_in_ready, data_stream_in_valid, data_stream_in_tlast;

    assign data_lut_out = data_lut_out2adj[STREAM_OUTPUT_WIDTH-1:0];
    axi_slave_lite_wrapper # (
        .C_S_AXI_DATA_WIDTH                     (STREAM_INPUT_WIDTH),
        .C_S_AXI_ADDR_WIDTH                     ($clog2(LUT_LEN)+4),
        .LUT_LEN                                (LUT_LEN),
        .AXI_LITE_ID                            (AXI_LITE_ID)
    )AXI_LITE_REG(
        .ADDR_LUT_IN                            (addr_lut_in),
        .ADDR_LUT_OUT                           (addr_lut_out),
        .DATA_LUT_IN                            (data_lut_in),
        .DATA_LUT_OUT                           (data_lut_out2adj),
        .DATA_LUT_IN_VALID                      (data_lut_in_valid),
        .DATA_LUT_OUT_VALID                     (data_lut_out_valid),
        .TABLE_LOAD                             (table_load), 
        .ENABLE_DEVICE                          (enable_device),
        // AXI Slave Lite signals
        .S_AXI_ACLK                             (ACLK),
        .S_AXI_ARESETN                          (ARESETN),
        .S_AXI_AWADDR                           (S_AXI_AWADDR),
        .S_AXI_AWPROT                           (S_AXI_AWPROT),
        .S_AXI_AWVALID                          (S_AXI_AWVALID),
        .S_AXI_AWREADY                          (S_AXI_AWREADY),
        .S_AXI_WDATA                            (S_AXI_WDATA),
        .S_AXI_WSTRB                            (S_AXI_WSTRB),
        .S_AXI_WVALID                           (S_AXI_WVALID),
        .S_AXI_WREADY                           (S_AXI_WREADY),
        .S_AXI_BRESP                            (S_AXI_BRESP),
        .S_AXI_BVALID                           (S_AXI_BVALID),
        .S_AXI_BREADY                           (S_AXI_BREADY),
        .S_AXI_ARADDR                           (S_AXI_ARADDR),
        .S_AXI_ARPROT                           (S_AXI_ARPROT),
        .S_AXI_ARVALID                          (S_AXI_ARVALID),
        .S_AXI_ARREADY                          (S_AXI_ARREADY),
        .S_AXI_RDATA                            (S_AXI_RDATA),
        .S_AXI_RRESP                            (S_AXI_RRESP),
        .S_AXI_RVALID                           (S_AXI_RVALID),
        .S_AXI_RREADY                           (S_AXI_RREADY)
    );

    lut_brightness_transform #(
        .STREAM_INPUT_WIDTH     (32),
        .STREAM_OUTPUT_WIDTH    (8),
        .LUT_LEN                (256)
    )lut_brightness(
        .CLK                    (ACLK),
        .nRST                   (ARESETN & enable_device),
        .DATA_STREAM_INPUT      (data_stream_in),
        .DATA_IN_VALID          (data_stream_in_valid),
        .DATA_STREAM_OUTPUT     (data_stream_out),
        .DATA_OUT_VALID         (data_stream_out_valid),
        // Input Stream LUT charge port
        .DATA_LUT_IN            (data_lut_in),
        .DATA_LUT_IN_VALID      (data_lut_in_valid),
        .ADDR_LUT_IN            (addr_lut_in),
        // Ouput Stream LUT charge port
        .DATA_LUT_OUT           (data_lut_out),
        .DATA_LUT_OUT_VALID     (data_lut_out_valid),
        .ADDR_LUT_OUT           (addr_lut_out),
        .LOAD_TABLE             (table_load)
    );


    axi_master_stream_wrapper # (
        .C_M_AXIS_TDATA_WIDTH   (STREAM_OUTPUT_WIDTH))
    AXI_STREAM_OUTPUT(
        .DATA_IN                (data_stream_out),
        .DATA_VALID             (data_stream_out_valid),
        .DATA_READY             (data_stream_out_ready),
        .DATA_TLAST             (data_stream_out_tlast),
        // AXI Stream signals
        .M_AXIS_ACLK            (ACLK),
        .M_AXIS_ARESETN         (ARESETN),
        .M_AXIS_TVALID          (M_AXIS_TVALID),
        .M_AXIS_TDATA           (M_AXIS_TDATA),
        .M_AXIS_TSTRB           (M_AXIS_TSTRB),
        .M_AXIS_TLAST           (M_AXIS_TLAST),
        .M_AXIS_TREADY          (M_AXIS_TREADY)
    );


    axi_slave_stream_wrapper # (
        .C_S_AXIS_TDATA_WIDTH  (STREAM_INPUT_WIDTH))
    AXI_STREAM_INPUT (
        .DATA_VALID             (data_stream_in_valid),
        .DATA_OUT               (data_stream_in),
        .DATA_READY             (data_stream_out_ready),
        .DATA_TLAST             (data_stream_in_tlast),
        // AXI Stream signals
        .S_AXIS_ACLK            (ACLK),
        .S_AXIS_ARESETN         (ARESETN),
        .S_AXIS_TREADY          (S_AXIS_TREADY),
        .S_AXIS_TDATA           (S_AXIS_TDATA),
        .S_AXIS_TLAST           (S_AXIS_TLAST),
        .S_AXIS_TVALID          (S_AXIS_TVALID)
    );


    bit_pipelined #(
        .STAGES         (17+STREAM_INPUT_WIDTH)
    )tlast_in_out_delayed(
        .CLK            (ACLK),
        .nRST           (ARESETN & enable_device),
        .DATA_IN        (data_stream_in_tlast),
        .DATA_OUT       (data_stream_out_tlast)
    );


`ifdef WAVEFORM_AXI_LUT_BRIGHTNESS_TRANSFORM
    initial begin
        $dumpfile("./sim_build/waveform.vcd");
        $dumpvars (0, axi_lut_brightness_transform);
    end
`endif

endmodule