///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description: 
//
// AXI Lite Slave based on xilinx implementation
// Author: David Caruso
//
////////////////////////////////////////////////////////////////////////////////

module axi_slave_lite_wrapper #
(
    parameter integer C_S_AXI_DATA_WIDTH    = 32,
    parameter integer C_S_AXI_ADDR_WIDTH    = 12,
    parameter integer LUT_LEN               = 256,
    parameter         AXI_LITE_ID           = 32'hCAFECAFE
)
(
    output [$clog2(LUT_LEN)-1:0]            ADDR_LUT_IN,
    output [$clog2(LUT_LEN)-1:0]            ADDR_LUT_OUT,
    output [C_S_AXI_DATA_WIDTH-1:0]         DATA_LUT_IN,
    output [C_S_AXI_DATA_WIDTH-1:0]         DATA_LUT_OUT, //only 8 bit lower are valid
    output                                  DATA_LUT_IN_VALID,
    output                                  DATA_LUT_OUT_VALID,
    output                                  TABLE_LOAD,
    output                                  ENABLE_DEVICE,
    // AXI Slave Lite signals
    input                                   S_AXI_ACLK,
    input                                   S_AXI_ARESETN,
    input [C_S_AXI_ADDR_WIDTH-1 : 0]        S_AXI_AWADDR,
    input [2 : 0]                           S_AXI_AWPROT,
    input                                   S_AXI_AWVALID,
    output                                  S_AXI_AWREADY,
    input [C_S_AXI_DATA_WIDTH-1 : 0]        S_AXI_WDATA,  
    input [(C_S_AXI_DATA_WIDTH/8)-1:0]      S_AXI_WSTRB,
    input                                   S_AXI_WVALID,
    output                                  S_AXI_WREADY,
    output [1:0]                            S_AXI_BRESP,
    output                                  S_AXI_BVALID,
    input                                   S_AXI_BREADY,
    input [C_S_AXI_ADDR_WIDTH-1:0]          S_AXI_ARADDR,
    input [2:0]                             S_AXI_ARPROT,
    input                                   S_AXI_ARVALID,
    output                                  S_AXI_ARREADY,
    output [C_S_AXI_DATA_WIDTH-1:0]         S_AXI_RDATA,
    output [1:0]                            S_AXI_RRESP,
    output                                  S_AXI_RVALID,
    input                                   S_AXI_RREADY
);
//////////////////////////////////////////////////////////
//   Memory Map:
//   0x00000000 ID
//   0x00000004 STATUS/CONTROL
//   0x00000400 LUT_IN
//   0x00000800 LUT_OUT
//////////////////////////////////////////////////////////

    localparam integer ADDR_REG_SEP = 2; // read at 32 bits, so 4 address of separation for each register
    localparam integer ADDR_LSB     = $clog2(LUT_LEN)+2;

    // AXI4LITE signals
    reg [C_S_AXI_ADDR_WIDTH-1 : 0]  axi_awaddr;
    reg                             axi_awready;
    reg                             axi_wready;
    reg [1 : 0]                     axi_bresp;
    reg                             axi_bvalid;
    reg [C_S_AXI_ADDR_WIDTH-1 : 0]  axi_araddr;
    reg                             axi_arready;
    reg [C_S_AXI_DATA_WIDTH-1 : 0]  axi_rdata;
    reg [1 : 0]                     axi_rresp;
    reg                             axi_rvalid;

    wire                            slv_reg_rden;
    wire                            slv_reg_wren;
    reg                             aw_en;
    reg [C_S_AXI_DATA_WIDTH-1:0]    ctrl_status_reg;
    wire [1:0]                      addr_wreg, addr_rreg;

    assign S_AXI_AWREADY    = axi_awready;
    assign S_AXI_WREADY     = axi_wready;
    assign S_AXI_BRESP      = axi_bresp;
    assign S_AXI_BVALID     = axi_bvalid;
    assign S_AXI_ARREADY    = axi_arready;
    assign S_AXI_RDATA      = axi_rdata;
    assign S_AXI_RRESP      = axi_rresp;
    assign S_AXI_RVALID     = axi_rvalid;

    assign ENABLE_DEVICE    = ctrl_status_reg[0];
    assign TABLE_LOAD       = ctrl_status_reg[1];

    always @( posedge S_AXI_ACLK )
    begin
        if ( S_AXI_ARESETN == 1'b0 ) begin
            axi_awready <= 1'b0;
            aw_en <= 1'b1;
        end 
        else begin    
            if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID && aw_en) begin
                axi_awready <= 1'b1;
                aw_en <= 1'b0;
            end
            else if (S_AXI_BREADY && axi_bvalid) begin
                aw_en <= 1'b1;
                axi_awready <= 1'b0;
            end
            else begin
                axi_awready <= 1'b0;
            end
        end 
    end       

    always @( posedge S_AXI_ACLK )
    begin
        if ( S_AXI_ARESETN == 1'b0 ) begin
            axi_awaddr <= 0;
        end 
        else begin    
            if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID && aw_en) begin
                axi_awaddr <= S_AXI_AWADDR;
            end
        end 
    end       

    always @( posedge S_AXI_ACLK ) begin
        if ( S_AXI_ARESETN == 1'b0 ) begin
            axi_wready <= 1'b0;
        end 
        else begin    
            if (~axi_wready && S_AXI_WVALID && S_AXI_AWVALID && aw_en ) begin
                axi_wready <= 1'b1;
            end
            else begin
                axi_wready <= 1'b0;
            end
        end 
    end       

    assign slv_reg_wren = axi_wready && S_AXI_WVALID && axi_awready && S_AXI_AWVALID;
    assign addr_wreg = axi_awaddr[ADDR_LSB+:2];

    always @(posedge S_AXI_ACLK) begin
        if (~S_AXI_ARESETN) begin
            ctrl_status_reg <= 'd0;
        end
        else if (slv_reg_wren) begin
            if (addr_wreg == 2'b00) begin
                ctrl_status_reg <= S_AXI_WDATA;
            end
        end
    end

    assign DATA_LUT_IN        = S_AXI_WDATA;
    assign DATA_LUT_OUT       = S_AXI_WDATA;
    assign ADDR_LUT_IN        = axi_awaddr[(ADDR_LSB-1)-:$clog2(LUT_LEN)];
    assign ADDR_LUT_OUT       = axi_awaddr[(ADDR_LSB-1)-:$clog2(LUT_LEN)];
    assign DATA_LUT_IN_VALID  = (addr_wreg==2'b01)? slv_reg_wren:1'b0;
    assign DATA_LUT_OUT_VALID = (addr_wreg==2'b10)? slv_reg_wren:1'b0;


    always @( posedge S_AXI_ACLK ) begin
        if ( S_AXI_ARESETN == 1'b0 ) begin
            axi_bvalid  <= 0;
            axi_bresp   <= 2'b0;
        end 
        else begin    
            if (axi_awready && S_AXI_AWVALID && ~axi_bvalid && axi_wready && S_AXI_WVALID) begin
                axi_bvalid <= 1'b1;
                axi_bresp  <= 2'b0; // 'OKAY' response 
            end                   // work error responses in future
            else begin
                if (S_AXI_BREADY && axi_bvalid) begin
                    axi_bvalid <= 1'b0; 
                end  
            end
        end
    end   

    always @( posedge S_AXI_ACLK ) begin
        if ( S_AXI_ARESETN == 1'b0 ) begin
            axi_arready <= 1'b0;
            axi_araddr  <= 32'b0;
        end 
        else begin    
            if (~axi_arready && S_AXI_ARVALID) begin
                axi_arready <= 1'b1;
                axi_araddr  <= S_AXI_ARADDR;
            end
            else begin
                axi_arready <= 1'b0;
            end
        end 
    end       

    always @( posedge S_AXI_ACLK ) begin
        if ( S_AXI_ARESETN == 1'b0 ) begin
            axi_rvalid <= 0;
            axi_rresp  <= 0;
        end 
        else begin    
            if (axi_arready && S_AXI_ARVALID && ~axi_rvalid) begin
                axi_rvalid <= 1'b1;
                axi_rresp  <= 2'b0; // 'OKAY' response
            end   
            else if (axi_rvalid && S_AXI_RREADY) begin
                axi_rvalid <= 1'b0;
            end                
        end
    end    

    assign slv_reg_rden = axi_arready & S_AXI_ARVALID & ~axi_rvalid;
    assign addr_rreg = (axi_araddr[ADDR_LSB+:2]==2'b00)? axi_araddr[ADDR_REG_SEP+:2]:2'b11;

    always @( posedge S_AXI_ACLK ) begin
        if ( S_AXI_ARESETN == 1'b0 ) begin
            axi_rdata  <= 0;
        end 
        else begin    
            if (slv_reg_rden) begin
                case (addr_rreg)
                    2'b00:
                        axi_rdata <= AXI_LITE_ID;
                    2'b01:
                        axi_rdata <= ctrl_status_reg;
                    default:
                        axi_rdata <= 'd0;
                endcase
            end   
        end
    end    

endmodule