///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description: 
//
// AXI Stream Slave (sink) based on xilinx implementation
// Author: David Caruso
//
////////////////////////////////////////////////////////////////////////////////

module axi_slave_stream_wrapper #
(
    parameter integer C_S_AXIS_TDATA_WIDTH  = 32
)
(
    output                                  DATA_VALID,
    output [C_S_AXIS_TDATA_WIDTH-1:0]       DATA_OUT,
    input                                   DATA_READY,
    output                                  DATA_TLAST,
    // AXI Stream signals
    input                                   S_AXIS_ACLK,
    input                                   S_AXIS_ARESETN,
    output                                  S_AXIS_TREADY,
    input  [C_S_AXIS_TDATA_WIDTH-1 : 0]     S_AXIS_TDATA,
    input                                   S_AXIS_TLAST,
    input                                   S_AXIS_TVALID
);

    assign DATA_OUT      = S_AXIS_TDATA;
    assign DATA_VALID    = S_AXIS_TVALID & S_AXIS_TREADY;
    assign S_AXIS_TREADY = DATA_READY;
    assign DATA_TLAST    = S_AXIS_TLAST;

endmodule