///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description:
//
// Register pipelined
// Author: David Caruso
//
////////////////////////////////////////////////////////////////////////////////

module reg_pipelined #(
    parameter STAGES = 2,
    parameter DATA_WIDTH = 32
)(
    input                                     CLK,
    input                                     nRST,
    input  [DATA_WIDTH-1:0]                   DATA_IN,
    output [DATA_WIDTH-1:0]                   DATA_OUT
);

reg [DATA_WIDTH-1:0]      data_r [0:STAGES-1];

integer idx;
always @(posedge CLK) begin
    if (~nRST) begin
        for (idx = 0; idx < STAGES; idx = idx + 1) begin
            data_r[idx] <= 'd0;
        end
    end
    else begin
        data_r[0] <= DATA_IN;
        if (STAGES > 1) begin
            for (idx = 1; idx < STAGES; idx = idx + 1) begin
                data_r[idx] <= data_r[idx-1];
            end
        end
    end
end
// 
// generate
    // if (STAGES > 1)
        assign DATA_OUT       = data_r[STAGES-1];
    // else 
        // assign DATA_OUT       = data_r;
// endgenerate

endmodule