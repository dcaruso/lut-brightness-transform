# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "AXI_LITE_ID" -parent ${Page_0}
  ipgui::add_param $IPINST -name "LUT_LEN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STREAM_INPUT_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STREAM_OUTPUT_WIDTH" -parent ${Page_0}


}

proc update_PARAM_VALUE.AXI_LITE_ID { PARAM_VALUE.AXI_LITE_ID } {
	# Procedure called to update AXI_LITE_ID when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AXI_LITE_ID { PARAM_VALUE.AXI_LITE_ID } {
	# Procedure called to validate AXI_LITE_ID
	return true
}

proc update_PARAM_VALUE.LUT_LEN { PARAM_VALUE.LUT_LEN } {
	# Procedure called to update LUT_LEN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LUT_LEN { PARAM_VALUE.LUT_LEN } {
	# Procedure called to validate LUT_LEN
	return true
}

proc update_PARAM_VALUE.STREAM_INPUT_WIDTH { PARAM_VALUE.STREAM_INPUT_WIDTH } {
	# Procedure called to update STREAM_INPUT_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STREAM_INPUT_WIDTH { PARAM_VALUE.STREAM_INPUT_WIDTH } {
	# Procedure called to validate STREAM_INPUT_WIDTH
	return true
}

proc update_PARAM_VALUE.STREAM_OUTPUT_WIDTH { PARAM_VALUE.STREAM_OUTPUT_WIDTH } {
	# Procedure called to update STREAM_OUTPUT_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STREAM_OUTPUT_WIDTH { PARAM_VALUE.STREAM_OUTPUT_WIDTH } {
	# Procedure called to validate STREAM_OUTPUT_WIDTH
	return true
}


proc update_MODELPARAM_VALUE.LUT_LEN { MODELPARAM_VALUE.LUT_LEN PARAM_VALUE.LUT_LEN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LUT_LEN}] ${MODELPARAM_VALUE.LUT_LEN}
}

proc update_MODELPARAM_VALUE.STREAM_INPUT_WIDTH { MODELPARAM_VALUE.STREAM_INPUT_WIDTH PARAM_VALUE.STREAM_INPUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STREAM_INPUT_WIDTH}] ${MODELPARAM_VALUE.STREAM_INPUT_WIDTH}
}

proc update_MODELPARAM_VALUE.STREAM_OUTPUT_WIDTH { MODELPARAM_VALUE.STREAM_OUTPUT_WIDTH PARAM_VALUE.STREAM_OUTPUT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STREAM_OUTPUT_WIDTH}] ${MODELPARAM_VALUE.STREAM_OUTPUT_WIDTH}
}

proc update_MODELPARAM_VALUE.AXI_LITE_ID { MODELPARAM_VALUE.AXI_LITE_ID PARAM_VALUE.AXI_LITE_ID } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AXI_LITE_ID}] ${MODELPARAM_VALUE.AXI_LITE_ID}
}

