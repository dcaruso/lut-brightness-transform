# LUT Brightness Transform

Simple IP Core to transform a 32 bit video stream into 8 bit video stream with brightness adjustments.
This device is like a gamma LUT correction, it receives a 32 bits signed stream and using a function stored
in a LUT, it generates an 8 bits unsigned stream. The core can process one sample per clock with a latency
of 15 cycles.
The video stream input and output are AXI Stream compatible.

## IP Core Architecture

The following picture show the architecture of the IP Core
![arch](figs/architecture_core.png)

### AXI Wrappers

The IP Core has 3 AXI wrappers to work with compatible AXI-4 protocols. Those wrappers allow using simpler internal signals.

* AXI Slave Stream wrapper: receives the input AXI stream (32 bits) and generates the local signals (without protocol).
* AXI Master Stream wrapper: generates the AXI stream output (8 bits) from local signals.
* AXI Slave Lite wrapper: implements the IP Core configuration registers.

#### AXI Lite Registers

| Register       | ADDR       | BIT | Description                                      |
|----------------|------------|-----|--------------------------------------------------|
| ID             | 0x00000000 |     | Read the fixed value 0xCAFECAFE                  |
| CONTROL/STATUS | 0x00000004 | 0   | ENABLE device (with '1')                         |
| CONTROL/STATUS | 0x00000004 | 1   | LOAD_TABLE ('1' means load table 1, use table 0) |
| LUT_IN         | 0x00000400 |     | Input table in 32 bits                           |
| LUT_OUT        | 0x00000800 |     | Output table in 8 bits (written in 32 bits)      |

### LUTs

There are two LUTs (TABLE 0 and TABLE 1) that contain the function *f(X)=Y*, where X is the signed 32 bits input and Y is the unsigned 8 bits expected output . The X values in the LUTs must be storaged monotonically increasing.

### LUT Seeker

This module searches the new input (*XI*) in the 32 bits LUT. The searching method used is "binary search".
As the core needs to find each new value for each clock cycle, a pipelined design was used inside this block. The lut_seeker needs 9 repeated LUTs
of 32 bits to do the binary quest. Each LUT is used on each pipeline stage. For binary search only 8 stages are needed, but sometimes the
new value (*XI*) is not present in the LUT, so the module find the values around (*XA* and *XB*). This module delivers the *XA*, *XB* and
*XI* values for each clock cycle. Knowing the address of *XA* and *XB* the values *YA* and *YB* could be found in the 8 bits LUT, because they have the same address.
Also this module solves the problem when the input value is out of LUT's values bounds.

### Interpolator

This module does the interpolation with a linear function to find the expected output for *f(XI)=YO*. It implements the following equation:

```math
YO = (XI-XA) \: {{YB-YA} \over {XB-XA}} + YA
```

To improve the module performance, this calcululation is performed in a pipeline.

Then, the YO value found is sent to the AXI Master Stream wrapper.


## Directories

```.

├── src
│   └── hdl (HDL IP Cores directory)
│       ├── axi_lut_brightness_transform.v (top level)
│       ├── axi_master_stream_wrapper.v
│       ├── axi_slave_lite_wrapper.v
│       ├── axi_slave_stream_wrapper.v
│       ├── bit_pipelined.v
│       ├── component.xml (IP repo for vivado)
│       ├── div_su.v
│       ├── div_uu.v
│       ├── dual_port_ram.v
│       ├── interpolator.v
│       ├── lut_brightness_transform.v
│       ├── lut_seeker.v
│       ├── reg_pipelined.v
│       └── xgui
│           └── axi_lut_brightness_transform_v1_0.tcl
│
├── test (testbenches done with CoCoTB)
│   ├── axi_lut_brightness_transform_tb
│   ├── div_tb
│   ├── interpolator_tb
│   ├── lut_brightness_transform_tb
│   └── lut_seeker_tb
└── utils (useful scripts)
    ├── buildimage.sh
    └── run_test_local.sh
```

## Software install for standalone test

To run the testbench you need [CoCoTB](https://cocotb.readthedocs.io/en/latest/introduction.html) installed in your computer,
or you could use the docker container to work with that.

### Clone repository

First of all, you need to clone this repository:

```
    $ git clone https://gitlab.com/dcaruso/lut-brightness-transform.git
```

### Installing Docker on linux (Debian/Ubuntu)
```
    $ sudo apt-get install apt-transport-https dirmngr
    $ sudo echo 'deb https://apt.dockerproject.org/repo debian-stretch main' >> /etc/apt/sources.list
    $ sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
    $ sudo apt-get update
    $ sudo apt-get install docker-engine
    $ sudo usermod -a -G docker $USER
```

### Build the docker image

```
    $ cd utils
    $ ./buildimage.sh
```
This bash script build the docker image and prepare all enviroment variables to work with CoCoTB.

### Run any testbench

To run each testbench you need to use the other bash tool called `run_test_local.sh`.

First enter to test directory:

```
    $ cd test/interpolator_tb/
```

Then run a single testbench like this

```
    $ ../../utils/run_test_local.sh make
```

If you want to generate the VCD waveforms, run the following command (waveforms are not generated by default ):

```
    $ ../../utils/run_test_local.sh "make EXTRA_ARGS=-DWAVEFORM_INTERPOLATOR"
```

## Automatic testbench on gitlab-ci

For each push in the repository, all testbenches are executed! You could see each [pipeline](https://gitlab.com/dcaruso/lut-brightness-transform/pipelines)
