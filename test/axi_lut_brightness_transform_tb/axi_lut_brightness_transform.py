###############################################################################
#
# This file is part of the lut_brightness_transform project
# (https://gitlab.com/dcaruso/lut-brightness-transform).
# Copyright (c) 2019 David M. Caruso.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
#
# Description:
#
# Python class for the AXI LUT Brightness Transform core
#
###############################################################################
import cocotb
import random
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue
from random import randint
from cocotb.drivers.amba import AXI4LiteMaster
from cocotb.drivers.amba import AXIProtocolError
from Queue import Queue

CLK_PERIOD = 1
ADDR_ID = 0
ADDR_CONTROL_STATUS = 4
ADDR_LUT_IN = 0x400
ADDR_LUT_OUT = 0x800
ENABLE_BIT = 0
LOAD_TABLE_BIT = 1

class AXIStreamSlave:
    def __init__(self, dut):
        self.dut = dut
        self.dut._log.info('Init AXI Stream for the output stream')
        self.dut.M_AXIS_TREADY <= 0
        self.fifo_out = Queue()

    @cocotb.coroutine
    def stream_output_collector(self):
        self.dut._log.info('Starting output collector')
        self.dut.M_AXIS_TREADY <= 1
        while(True):
            yield RisingEdge(self.dut.ACLK)
            if (self.dut.M_AXIS_TVALID.value):
                self.fifo_out.put(self.dut.M_AXIS_TDATA.value.integer)

    def get_output_collected(self):
        outputs = []
        for i in range(self.fifo_out.qsize()):
            outputs.append(self.fifo_out.get(block=None))
        return outputs

class AXIStreamMaster:
    def __init__(self, dut):
        self.dut = dut
        self.dut._log.info('Init AXI Stream for the input stream')
        self.dut.S_AXIS_TDATA <= 0
        self.dut.S_AXIS_TLAST <= 0
        self.dut.S_AXIS_TVALID <= 0
        self.fifo_in = Queue()
        cocotb.fork(self.stream_input_generator())

    def new_value_to_fifo(self, value):
        self.fifo_in.put(value)

    def fifo_in_empty(self):
        return self.fifo_in.empty()

    @cocotb.coroutine
    def stream_input_generator(self):
        self.dut._log.info('Starting the input generator stream')
        while(True):
            yield RisingEdge(self.dut.ACLK)
            while(self.fifo_in.empty()):
                self.dut.S_AXIS_TLAST <= 0
                self.dut.S_AXIS_TVALID <= 0
                yield RisingEdge(self.dut.ACLK)
            while(self.dut.S_AXIS_TREADY.value==0):
                yield RisingEdge(self.dut.ACLK)
            if self.fifo_in.qsize() == 1:
                self.dut.S_AXIS_TLAST <= 1
            else:
                self.dut.S_AXIS_TLAST <= 0
            self.dut.S_AXIS_TVALID <= 1
            self.dut.S_AXIS_TDATA <= self.fifo_in.get()

class AXILutBrightnessTransform:
    def __init__(self, dut):
        self.dut = dut
        self.dut._log.info('Init Device Under Test (DUT)')
        self.axi_reg = AXI4LiteMaster(dut, "S_AXI", dut.ACLK)
        cocotb.fork(Clock(self.dut.ACLK, CLK_PERIOD, units='ns').start())
        self.axi_stream_in = AXIStreamMaster(dut)
        self.axi_stream_out = AXIStreamSlave(dut)
        self.READ_TABLE = 1
        self.LOAD_TABLE = 0
        self.LUT_LEN = self.dut.LUT_LEN.value.integer
        self.WIDTH_IN = self.dut.STREAM_INPUT_WIDTH.value.integer
        self.WIDTH_OUT = self.dut.STREAM_OUTPUT_WIDTH.value.integer
        self.lut_in = [[0] * self.LUT_LEN, [0] * self.LUT_LEN]
        self.lut_out = [[0] * self.LUT_LEN, [0] * self.LUT_LEN]
        self.DATA_OUT = []
        self.XA_d = []
        self.XB_d = []
        self.YA_d = []
        self.YB_d = []
        self.XI_d = []
        self.ADDRA_d = []
        self.ADDRB_d = []

    @cocotb.coroutine
    def reset(self):
        self.dut._log.info('Reset DUT')
        self.dut.ARESETN <= 0
        for i in range(10):
            yield RisingEdge(self.dut.ACLK)
        self.dut.ARESETN <= 1

    @cocotb.coroutine
    def get_id(self):
        id_read = yield self.axi_reg.read(ADDR_ID)
        raise ReturnValue(id_read)

    @cocotb.coroutine
    def write_bit_axi_lite(self, reg, bit, value):
        reg_value = yield self.axi_reg.read(reg)
        yield RisingEdge(self.dut.ACLK)
        if value==1:
            yield self.axi_reg.write(reg, reg_value.integer | (1<<bit))
        else:
            yield self.axi_reg.write(reg, reg_value.integer & (~(1<<bit)))

    @cocotb.coroutine
    def set_device_status(self, status):
        if status=='enable':
            self.dut._log.info("Enable device")
            yield self.write_bit_axi_lite(ADDR_CONTROL_STATUS, ENABLE_BIT, 1)
        else:
            self.dut._log.info("Disable device")
            yield self.write_bit_axi_lite(ADDR_CONTROL_STATUS, ENABLE_BIT, 0)

    @cocotb.coroutine
    def select_work_table(self, table):
        yield self.write_bit_axi_lite(ADDR_CONTROL_STATUS, LOAD_TABLE_BIT, 1-table)
        self.LOAD_TABLE = 1-table
        self.READ_TABLE = table

    @cocotb.coroutine
    def fill_input_lut(self, value, addr):
        yield self.axi_reg.write((addr*4)+ADDR_LUT_IN, value)
        self.lut_in[self.LOAD_TABLE][addr] = value

    @cocotb.coroutine
    def fill_output_lut(self, value, addr):
        yield self.axi_reg.write((addr*4)+ADDR_LUT_OUT, value)
        self.lut_out[self.LOAD_TABLE][addr] = value

    def new_input(self, value):
        self.axi_stream_in.new_value_to_fifo(value)

    def get_lut_out_value(self, addr):
        if addr >= self.LUT_LEN:
            return self.lut_out[self.READ_TABLE][self.LUT_LEN-1]
        if addr < 0:
            return self.lut_out[self.READ_TABLE][0]
        return self.lut_out[self.READ_TABLE][addr]

    def get_lut_in_value(self, addr):
        if addr >= self.LUT_LEN:
            return self.lut_in[self.READ_TABLE][self.LUT_LEN-1]
        if addr < 0:
            return self.lut_in[self.READ_TABLE][0]
        return self.lut_in[self.READ_TABLE][addr]

    @cocotb.coroutine
    def create_new_function_lut(self, table):
        yield self.select_work_table(1-table)
        value_in = random.randint(-(2**(self.WIDTH_IN-1)), (2**(self.WIDTH_IN-1)-1))
        value_out = random.randint(0, (2**(self.WIDTH_OUT))-1)
        for i in range(self.LUT_LEN):
            yield self.fill_input_lut(value_in, i)
            yield self.fill_output_lut(value_out, i)
            if value_in < (2**(self.WIDTH_IN-1)-1):
                value_in = random.randint(value_in+1, (2**(self.WIDTH_IN-1)-1))
            value_out = random.randint(0, (2**(self.WIDTH_OUT))-1)

    def get_output_collected(self):
        return self.axi_stream_out.get_output_collected()

    def get_used_function(self):
        return self.lut_in[self.READ_TABLE], self.lut_out[self.READ_TABLE]
