###############################################################################
#
# This file is part of the lut_brightness_transform project
# (https://gitlab.com/dcaruso/lut-brightness-transform).
# Copyright (c) 2019 David M. Caruso.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
#
# Description:
#
# Testbench for the AXI LUT Brightness Transform core
#
###############################################################################
import cocotb
import random
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.regression import TestFactory
from cocotb.binary import BinaryValue
from random import randint
from axi_lut_brightness_transform import *

def Interpolation(XA, XB, XI, YA, YB):
    if (XA==XB):
        return int(YA)
    else:
        YO = int(((1.0*(XI-XA)*(YB-YA)))/(XB-XA)) + YA
        return YO

def lut_seeker(lut_len, dev, xi):
    for i in range(lut_len):
        if xi == dev.get_lut_in_value(i):
            xa = dev.get_lut_in_value(i)
            xb = xa
            ya = dev.get_lut_out_value(i)
            yb = ya
            return xa, xb, xi, ya, yb
        if xi <= dev.get_lut_in_value(i):
            xa = dev.get_lut_in_value(i-1)
            xb = dev.get_lut_in_value(i)
            ya = dev.get_lut_out_value(i-1)
            yb = dev.get_lut_out_value(i)
            return xa, xb, xi, ya, yb
    xa = dev.get_lut_in_value(lut_len)
    xb = dev.get_lut_in_value(lut_len)
    ya = dev.get_lut_out_value(lut_len)
    yb = dev.get_lut_out_value(lut_len)
    return xa, xb, xi, ya, yb

def get_new_output(lut_len, dev, xi):
    xa, xb, xi, ya, yb = lut_seeker(lut_len, dev, xi)
    YO = Interpolation(xa, xb, xi, ya, yb)
    return YO

@cocotb.test()
def Check_ID(dut):
    """
        Check ID for AXI lite device
    """
    dev = AXILutBrightnessTransform(dut)
    yield dev.reset()
    id_read = yield dev.axi_reg.read(0)
    if dut.AXI_LITE_ID.value.integer != id_read.integer:
        raise TestFailure("The ID was not readed well: needed {:0X} but was {:0X}".format(dut.AXI_LITE_ID.value.integer, read.integer))

@cocotb.test()
def Check_ID(dut):
    """
        Check ID for AXI lite device
    """
    dev = AXILutBrightnessTransform(dut)
    yield dev.reset()
    id_read = yield dev.get_id()
    if dut.AXI_LITE_ID.value.integer != id_read.integer:
        raise TestFailure("The ID was not readed well: needed {:0X} but was {:0X}".format(dut.AXI_LITE_ID.value.integer, read.integer))

@cocotb.test()
def Check_enable(dut):
    """
        Check the enable function
    """
    dev = AXILutBrightnessTransform(dut)
    yield dev.reset()
    dut._log.info("Check the initial state of the device")
    if dut.lut_brightness.nRST.value != 0:
        raise TestFailure("The device is not reseted")
    yield dev.set_device_status('enable')
    if dut.lut_brightness.nRST.value != 1:
        raise TestFailure("The device is not enabled")
    yield dev.set_device_status('disable')
    if dut.lut_brightness.nRST.value != 0:
        raise TestFailure("The device is not disabled")

@cocotb.test()
def Check_table_selector(dut):
    """
        Check the load table/uses table function
    """
    dev = AXILutBrightnessTransform(dut)
    yield dev.reset()
    yield dev.set_device_status('enable')
    yield dev.select_work_table(0)
    if dut.lut_brightness.LOAD_TABLE.value != 1:
        raise TestFailure("The load table needs to be the 1!")
    yield dev.select_work_table(1)
    if dut.lut_brightness.LOAD_TABLE.value != 0:
        raise TestFailure("The load table needs to be the 0!")

@cocotb.coroutine
def Streaming_mode_test(dut, table):
    """
        Do a continuous streaming test
    """
    dev = AXILutBrightnessTransform(dut)
    yield dev.reset()
    cocotb.fork(dev.axi_stream_out.stream_output_collector())
    yield dev.set_device_status('enable')
    yield dev.create_new_function_lut(table)
    yield dev.select_work_table(table)
    DATA_OUT = []
    # DATA_IN = []
    for i in range(10000):
        XI = random.randint(-(2**(dev.WIDTH_IN-1)), (2**(dev.WIDTH_IN-1)-1))
        dev.new_input(XI)
        YO = get_new_output(dev.LUT_LEN, dev, XI)
        DATA_OUT.append(YO)
    yield RisingEdge(dut.ACLK)
    while(dut.M_AXIS_TLAST.value==0):
        yield RisingEdge(dut.ACLK)
    yield RisingEdge(dut.ACLK)
    DATA_OUT_dev = dev.get_output_collected()

    if (DATA_OUT != DATA_OUT_dev):
        diff = list(set(DATA_OUT) - set(DATA_OUT_dev))
        raise TestFailure("Interpolator fails, Difference {}, elements differ {}".format(diff, len(diff)))

factory = TestFactory(Streaming_mode_test)
factory.add_option("table", [0, 1])
factory.generate_tests()
