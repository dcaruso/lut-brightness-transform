###############################################################################
#
# This file is part of the lut_brightness_transform project
# (https://gitlab.com/dcaruso/lut-brightness-transform).
# Copyright (c) 2019 David M. Caruso.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
#
# Description:
#
# Testbench for Interpolator core
#
###############################################################################

import cocotb
import random
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue
from random import randint
from interpolator import *

def Interpolation(XA, XB, XI, YA, YB):
    YO = int(((1.0*(XI-XA)*(YB-YA)))/(XB-XA)) + YA
    return YO

def Get_new_values(dut):
    INPUT_W = dut.X_WIDTH.value.integer
    OUTPUT_W = dut.Y_WIDTH.value.integer
    XA = random.randint(-(2**(INPUT_W-1)), (2**(INPUT_W-1)-1))
    YA = random.randint(0, (2**(OUTPUT_W))-1)
    XB = random.randint(XA, (2**(INPUT_W-1))-1)
    YB = random.randint(YA, (2**OUTPUT_W)-1)
    XI = random.randint(XA, XB)
    return XA, XB, XI, YA, YB

@cocotb.test()
def Simple_test(dut):
    """
        Simple operation test
    """
    dev = Interpolator(dut)
    yield dev.reset()
    INPUT_W = dut.X_WIDTH.value.integer
    OUTPUT_W = dut.Y_WIDTH.value.integer
    XA = -(2**(INPUT_W-1))
    XB = (2**(INPUT_W-1)-1)
    XI = 10
    YA = 254
    YB = 0
    yield dev.setup_inputs(XA, XB, XI, YA, YB)
    yield dev.stop_interpolation()
    YO = Interpolation(XA, XB, XI, YA, YB)
    while(dut.OUTPUT_VALID.value!=1):
        yield RisingEdge(dut.CLK)

    if YO != dut.YO.value.integer:
        raise TestFailure("Interpolator fails, needed {} but was {}".format(YO, dut.YO.value.integer))

@cocotb.test()
def random_pipelined_test(dut):
    """
        Random Pipelined test
    """
    dev = Interpolator(dut)
    yield dev.reset()
    cocotb.fork(dev.output_collector())
    YO = []
    for _ in range(1000):
        XA, XB, XI, YA, YB = Get_new_values(dut)
        yield dev.setup_inputs(XA, XB, XI, YA, YB)
        YO.append(Interpolation(XA, XB, XI, YA, YB))
    yield dev.stop_interpolation()
    while(len(dev.get_output_collected())< len(YO)):
        yield RisingEdge(dut.CLK)

    YO_dev = dev.get_output_collected()
    if (YO != YO_dev):
        for i in range(len(YO)):
            if (YO[i]!=YO_dev[i]):
                dut._log.info("Difference: sim {}, dev {}".format(YO[i], YO_dev[i]))
        raise TestFailure("Interpolator fails, Difference {}".format(list(set(YO) - set(YO_dev))))

@cocotb.test()
def random_pipelined_interrupted_test(dut):
    """
        Random Pipelined test with interruptions
    """
    dev = Interpolator(dut)
    yield dev.reset()
    cocotb.fork(dev.output_collector())
    loop = random.randint(0, 100)
    step = random.randint(0, 10)
    interruption = random.randint(0, 80)
    YO = []
    for _ in range(loop):
        for _ in range(step):
            XA, XB, XI, YA, YB = Get_new_values(dut)
            yield dev.setup_inputs(XA, XB, XI, YA, YB)
            YO.append(Interpolation(XA, XB, XI, YA, YB))
        yield dev.stop_interpolation()
        for _ in range(interruption):
            yield RisingEdge(dut.CLK)
    while(len(dev.get_output_collected())< len(YO)):
        yield RisingEdge(dut.CLK)

    YO_dev = dev.get_output_collected()
    if (YO != YO_dev):
        raise TestFailure("Interpolator fails, Difference {}".format(list(set(YO) - set(YO_dev))))