###############################################################################
#
# This file is part of the lut_brightness_transform project
# (https://gitlab.com/dcaruso/lut-brightness-transform).
# Copyright (c) 2019 David M. Caruso.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
#
# Description:
#
# Python class for the Interpolator
#
###############################################################################

import cocotb
import random
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue
from random import randint

CLK_PERIOD = 1

class Interpolator:
    def __init__(self, dut):
        self.dut = dut
        self.dut._log.info('Init Device Under Test (DUT)')
        self.dut._log.info('Init Clock')
        cocotb.fork(Clock(self.dut.CLK, CLK_PERIOD, units='ns').start())
        self.dut.XI <= 0
        self.dut.XA <= 0
        self.dut.XB <= 0
        self.dut.YA <= 0
        self.dut.YB <= 0
        self.dut.DO_INTERPOLATION <= 0
        self.YO = []

    @cocotb.coroutine
    def reset(self):
        self.dut._log.info('Reset DUT')
        self.dut.nRST <= 0
        for i in range(10):
            yield RisingEdge(self.dut.CLK)
        self.dut.nRST <= 1

    @cocotb.coroutine
    def setup_inputs(self, XA, XB, XI, YA, YB):
        self.dut.XA <= XA
        self.dut.XB <= XB
        self.dut.XI <= XI
        self.dut.YA <= YA
        self.dut.YB <= YB
        self.dut.DO_INTERPOLATION <= 1
        yield RisingEdge(self.dut.CLK)

    @cocotb.coroutine
    def stop_interpolation(self):
        self.dut.DO_INTERPOLATION  <= 0
        yield RisingEdge(self.dut.CLK)

    @cocotb.coroutine
    def output_collector(self):
        self.dut._log.info('Starting output collector')
        while(True):
            if (self.dut.OUTPUT_VALID.value):
                self.YO.append(self.dut.YO.value.integer)
            yield RisingEdge(self.dut.CLK)

    def get_output_collected(self):
        return self.YO
