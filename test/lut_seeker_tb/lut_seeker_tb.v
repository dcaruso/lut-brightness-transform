///////////////////////////////////////////////////////////////////////////////
//
// This file is part of the lut_brightness_transform project
// (https://gitlab.com/dcaruso/lut-brightness-transform).
// Copyright (c) 2019 David M. Caruso.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////
// Description: 
//
// LUT seeker testbench top level
// Author: David Caruso
//
////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ps

module lut_seeker_tb #(
    parameter STREAM_INPUT_WIDTH = 32,
    parameter STREAM_OUTPUT_WIDTH = 8,
    parameter LUT_LEN = 256
)(
    input                                    CLK,
    input                                    nRST,
    input         [STREAM_INPUT_WIDTH-1:0]   DATA_STREAM_INPUT,
    input                                    DATA_IN_VALID,
    // Input Stream LUT charge port
    input         [STREAM_INPUT_WIDTH-1:0]   DATA_LUT_IN,
    input                                    DATA_LUT_IN_VALID,
    input         [$clog2(LUT_LEN)-1:0]      ADDR_LUT_IN,
    // Output from core
    output        [$clog2(LUT_LEN)-1:0]      XA_ADDR_FOUND,
    output        [$clog2(LUT_LEN)-1:0]      XB_ADDR_FOUND,
    output        [STREAM_INPUT_WIDTH-1:0]   XA_VALUE_FOUND,
    output        [STREAM_INPUT_WIDTH-1:0]   XB_VALUE_FOUND,
    output                                   FOUND
);

localparam STAGES_QUEST   = $clog2(LUT_LEN)+1;
localparam LUT_ADDR_WIDTH = $clog2(LUT_LEN);
wire [LUT_ADDR_WIDTH-1:0]                  addr_lut[0:STAGES_QUEST-1];
wire [STREAM_INPUT_WIDTH-1:0]              data_lut[0:STAGES_QUEST-1];
wire [LUT_ADDR_WIDTH*STAGES_QUEST-1:0]     addr_lut_pack;
wire [STREAM_INPUT_WIDTH*STAGES_QUEST-1:0] data_lut_pack;

wire signed [STREAM_INPUT_WIDTH-1:0]       xi_value_found, xa_value_found, xb_value_found;
reg signed [STREAM_INPUT_WIDTH-1:0]        xi_value_c, xa_value_c, xb_value_c;
wire [LUT_ADDR_WIDTH-1:0]                  xi_addr_found, xa_addr_found, xb_addr_found;
wire                                       found;
reg                                        found_r;

wire [STREAM_OUTPUT_WIDTH-1:0]             ya_value_found, yb_value_found;
reg  [STREAM_OUTPUT_WIDTH-1:0]             ya_value_c, yb_value_c;

genvar unpack_addr_idx;
generate
  for (unpack_addr_idx=0; unpack_addr_idx<(STAGES_QUEST); unpack_addr_idx=unpack_addr_idx+1) begin
    assign addr_lut[unpack_addr_idx][((LUT_ADDR_WIDTH)-1):0] = addr_lut_pack[(LUT_ADDR_WIDTH)*unpack_addr_idx +: LUT_ADDR_WIDTH];
  end
endgenerate

genvar pack_data_idx;

generate
  for (pack_data_idx=0; pack_data_idx<(STAGES_QUEST); pack_data_idx=pack_data_idx+1) begin
    assign data_lut_pack[(STREAM_INPUT_WIDTH)*pack_data_idx +: STREAM_INPUT_WIDTH] = data_lut[pack_data_idx][((STREAM_INPUT_WIDTH)-1):0];
  end
endgenerate

genvar i;
generate
    for (i=0; i<STAGES_QUEST; i=i+1) begin: gen_lut_for_stages
        dual_port_ram #(
            .DATA_WIDTH         (STREAM_INPUT_WIDTH),
            .MEMORY_SIZE        (LUT_LEN)
        )ram_input_stream(
            .CLKA               (CLK),
            .CLKB               (CLK),
            .WEA                (DATA_LUT_IN_VALID),
            .ADDRA              (ADDR_LUT_IN),
            .ADDRB              (addr_lut[i]),
            .DATA_A             (DATA_LUT_IN),
            .DATA_B             (data_lut[i])
        );
    end
endgenerate


    lut_seeker #(
        .X_WIDTH            (STREAM_INPUT_WIDTH),
        .LUT_LEN            (LUT_LEN),
        .STAGES_QUEST       (STAGES_QUEST)
    )lut_seeker_device(
        .CLK                (CLK),
        .nRST               (nRST),
        .XI_VALID           (DATA_IN_VALID),
        .XI_VALUE           (DATA_STREAM_INPUT),
        .FOUND              (FOUND),
        .XA_VALUE_FOUND     (XA_VALUE_FOUND),
        .XA_ADDR_FOUND      (XA_ADDR_FOUND),
        .XB_VALUE_FOUND     (XB_VALUE_FOUND),
        .XB_ADDR_FOUND      (XB_ADDR_FOUND),
        .ADDR_LUTS          (addr_lut_pack),
        .DATA_LUTS          (data_lut_pack)
    );


`ifdef WAVEFORM_LUT_SEEKER_TB
    initial begin
        $dumpfile("./sim_build/waveform.vcd");
        $dumpvars (0, lut_seeker_tb);
    end
`endif

endmodule