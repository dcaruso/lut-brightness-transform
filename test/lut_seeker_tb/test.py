###############################################################################
#
# This file is part of the lut_brightness_transform project
# (https://gitlab.com/dcaruso/lut-brightness-transform).
# Copyright (c) 2019 David M. Caruso.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
#
# Description:
#
# Testbench for LUT Seeker core
#
###############################################################################

import cocotb
import random
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue
from random import randint
from lut_seeker import *

def lut_seeker(lut_len, dev, xi):
    for i in range(lut_len):
        if xi == dev.get_lut_in_value(i):
            xa = dev.get_lut_in_value(i)
            xb = xa
            return xa, xb, i, i
        if xi <= dev.get_lut_in_value(i):
            xa = dev.get_lut_in_value(i-1)
            xb = dev.get_lut_in_value(i)
            addra = (i-1)
            if addra <0:
                addra = 0
            addrb = i
            return xa, xb, addra, addrb
    xa = dev.get_lut_in_value(lut_len)
    xb = xa
    return xa, xb, (lut_len-1), (lut_len-1)

@cocotb.test()
def random_values_in_streaming(dut):
    """
        Found random values in the LUT for streaming mode
    """
    dev = LutSeeker(dut)
    yield dev.reset()
    yield dev.create_new_function_lut()
    cocotb.fork(dev.output_collector())
    XA_t = []
    XB_t = []
    ADDRA_t = []
    ADDRB_t = []
    for i in range(100):
        XI = random.randint(-(2**(dev.WIDTH_IN-1)), (2**(dev.WIDTH_IN-1)-1))
        yield dev.new_input(XI)
        XA, XB, ADDRA, ADDRB = lut_seeker(dev.LUT_LEN, dev, XI)
        XA_t.append(XA)
        XB_t.append(XB)
        ADDRA_t.append(ADDRA)
        ADDRB_t.append(ADDRB)
    while(dut.lut_seeker_device.valid_r.value.integer!=0): # Wait for empty internal pipeline
        yield RisingEdge(dut.CLK)
    XA_d, XB_d, ADDRA_d, ADDRB_d = dev.get_output_collected()
    if ADDRA_d != ADDRA_t:
        raise TestFailure("ADDRA fails, \nneeded {} \nbut was {}".format(ADDRA_t, ADDRA_d))
    if ADDRB_d != ADDRB_t:
        raise TestFailure("ADDRB fails, \nneeded {} \nbut was {}".format(ADDRB_t, ADDRB_d))
    if XA_d != XA_t:
        raise TestFailure("XA fails, \nneeded {} \nbut was {}".format(XA_t, XA_d))
    if XB_d != XB_t:
        raise TestFailure("XB fails, \nneeded {} \nbut was {}".format(XB_t, XB_d))

@cocotb.test()
def random_values_in_streaming_interrupted(dut):
    """
        Found random values in the LUT for streaming mode with random interruptions
    """
    dev = LutSeeker(dut)
    yield dev.reset()
    yield dev.create_new_function_lut()
    cocotb.fork(dev.output_collector())
    loop = random.randint(0, 100)
    step = random.randint(0, 10)
    interruption = random.randint(0, 10)
    XA_t = []
    XB_t = []
    ADDRA_t = []
    ADDRB_t = []

    for _ in range(loop):
        for _ in range(step):
            XI = random.randint(-(2**(dev.WIDTH_IN-1)), (2**(dev.WIDTH_IN-1)-1))
            yield dev.new_input(XI)
            XA, XB, ADDRA, ADDRB = lut_seeker(dev.LUT_LEN, dev, XI)
            XA_t.append(XA)
            XB_t.append(XB)
            ADDRA_t.append(ADDRA)
            ADDRB_t.append(ADDRB)
        for _ in range(interruption):
            yield RisingEdge(dut.CLK)

    while(dut.lut_seeker_device.valid_r.value.integer!=0): # Wait for empty internal pipeline
        yield RisingEdge(dut.CLK)

    XA_d, XB_d, ADDRA_d, ADDRB_d = dev.get_output_collected()
    if ADDRA_d != ADDRA_t:
        raise TestFailure("ADDRA fails, \nneeded {} \nbut was {}".format(ADDRA_t, ADDRA_d))
    if ADDRB_d != ADDRB_t:
        raise TestFailure("ADDRB fails, \nneeded {} \nbut was {}".format(ADDRB_t, ADDRB_d))
    if XA_d != XA_t:
        raise TestFailure("XA fails, \nneeded {} \nbut was {}".format(XA_t, XA_d))
    if XB_d != XB_t:
        raise TestFailure("XB fails, \nneeded {} \nbut was {}".format(XB_t, XB_d))

@cocotb.test()
def linear_function_in_streaming(dut):
    """
        Found linear values in the LUT
    """
    dev = LutSeeker(dut)
    yield dev.reset()
    yield dev.create_linear_function_lut()
    cocotb.fork(dev.output_collector())
    XA_t = []
    XB_t = []
    ADDRA_t = []
    ADDRB_t = []
    for i in range(100):
        XI = random.randint(0, dev.LUT_LEN-1)
        yield dev.new_input(XI)
        XA, XB, ADDRA, ADDRB = lut_seeker(dev.LUT_LEN, dev, XI)
        XA_t.append(XA)
        XB_t.append(XB)
        ADDRA_t.append(ADDRA)
        ADDRB_t.append(ADDRB)
    while(dut.lut_seeker_device.valid_r.value.integer!=0): # Wait for empty internal pipeline
        yield RisingEdge(dut.CLK)
    XA_d, XB_d, ADDRA_d, ADDRB_d = dev.get_output_collected()
    if ADDRA_d != ADDRA_t:
        raise TestFailure("ADDRA fails, \nneeded {} \nbut was {}".format(ADDRA_t, ADDRA_d))
    if ADDRB_d != ADDRB_t:
        raise TestFailure("ADDRB fails, \nneeded {} \nbut was {}".format(ADDRB_t, ADDRB_d))
    if XA_d != XA_t:
        raise TestFailure("XA fails, \nneeded {} \nbut was {}".format(XA_t, XA_d))
    if XB_d != XB_t:
        raise TestFailure("XB fails, \nneeded {} \nbut was {}".format(XB_t, XB_d))