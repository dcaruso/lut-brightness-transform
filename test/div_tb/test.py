###############################################################################
#
# This file is part of the lut_brightness_transform project
# (https://gitlab.com/dcaruso/lut-brightness-transform).
# Copyright (c) 2019 David M. Caruso.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
#
# Description:
#
# Testbench for LUT Seeker core
#
###############################################################################

import cocotb
import random
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue
from random import randint

@cocotb.coroutine
def reset(dut):
    dut.nRST <= 0
    dut.INPUT_VALID <= 0
    dut.DIVIDEND <= 0
    dut.DIVISOR <= 0
    for i in range(10):
        yield RisingEdge(dut.CLK)
    dut.nRST <= 1

def get_new_numbers(dut):
    R = dut.DIVIDEND_WIDTH.value.integer
    LIMIT = 2**(dut.DIVISOR_WIDTH.value.integer-1)-1
    while(True):
        dividend_r = random.randint(-(2**(R-1)), (2**(R-1)-1))
        divisor_r = random.randint(0, (2**(R/2))-1)
        if divisor_r!=0:
            quotient_r = int((1.0*dividend_r) / divisor_r)
            if (abs(quotient_r))< LIMIT:
                return dividend_r, divisor_r, quotient_r

@cocotb.test()
def simple_test(dut):
    """
        Do a division
    """
    cocotb.fork(Clock(dut.CLK, 1, units='ns').start())
    yield reset(dut)
    dividend, divisor, quotient = get_new_numbers(dut)
    dut._log.info("Do division: dividend={}, divisor={}, expected quotient={}".format(dividend, divisor, quotient))
    dut.DIVIDEND <= dividend
    dut.DIVISOR <= divisor
    dut.INPUT_VALID <= 1
    while(dut.OUTPUT_VALID.value!=1):
        yield RisingEdge(dut.CLK)
    if dut.QUOTIENT.value.signed_integer != quotient:
        raise TestFailure("Not match: needed {}, was {}".format(quotient, dut.QUOTIENT.value.signed_integer))

@cocotb.test()
def streaming_test(dut):
    """
        Do streaming test
    """
    cocotb.fork(Clock(dut.CLK, 1, units='ns').start())
    yield reset(dut)
    dut.INPUT_VALID <= 1
    Q_e = []
    Q_d = []
    for i in range(1000):
        dividend, divisor, quotient = get_new_numbers(dut)
        Q_e.append(quotient)
        dut.DIVIDEND <= dividend
        dut.DIVISOR <= divisor
        if (dut.OUTPUT_VALID.value):
            Q_d.append(dut.QUOTIENT.value.signed_integer)
        yield RisingEdge(dut.CLK)
    dut.INPUT_VALID <= 0
    while(dut.OUTPUT_VALID.value):
        Q_d.append(dut.QUOTIENT.value.signed_integer)
        yield RisingEdge(dut.CLK)
    if  Q_e != Q_d:
        raise TestFailure("Not match: needed {}\nwas {}".format(Q_e, Q_d))

@cocotb.test()
def division_by_zero_test(dut):
    """
        test division by zero
    """
    cocotb.fork(Clock(dut.CLK, 1, units='ns').start())
    yield reset(dut)
    dividend, divisor, quotient = get_new_numbers(dut)
    dut.DIVIDEND <= dividend
    dut.DIVISOR <= 0
    dut.INPUT_VALID <= 1
    while(dut.OUTPUT_VALID.value!=1):
        yield RisingEdge(dut.CLK)
    if dut.DIV_BY_ZERO.value==0:
        raise TestFailure("Not alert divided by zero!")

@cocotb.test()
def test_one_value(dut):
    """
        Insert only one value
    """
    cocotb.fork(Clock(dut.CLK, 1, units='ns').start())
    yield reset(dut)
    dividend, divisor, quotient = get_new_numbers(dut)
    dut._log.info("Do division: dividend={}, divisor={}, expected quotient={}".format(dividend, divisor, quotient))
    dut.DIVIDEND <= dividend
    dut.DIVISOR <= divisor
    dut.INPUT_VALID <= 1
    yield RisingEdge(dut.CLK)
    dut.INPUT_VALID <= 0
    while(dut.OUTPUT_VALID.value!=1):
        yield RisingEdge(dut.CLK)
    if dut.QUOTIENT.value.signed_integer != quotient:
        raise TestFailure("Not match: needed {}, was {}".format(quotient, dut.QUOTIENT.value.signed_integer))


@cocotb.test()
def streaming_interrupted_test(dut):
    """
        Do streaming test interrupted
    """
    cocotb.fork(Clock(dut.CLK, 1, units='ns').start())
    yield reset(dut)
    dut.INPUT_VALID <= 1
    loop = random.randint(0, 100)
    step = random.randint(0, 10)
    interruption = random.randint(0, 60)
    Q_e = []
    Q_d = []
    for _ in range(loop):
        for _ in range(step):
            dut.INPUT_VALID <= 1
            dividend, divisor, quotient = get_new_numbers(dut)
            Q_e.append(quotient)
            dut.DIVIDEND <= dividend
            dut.DIVISOR <= divisor
            if (dut.OUTPUT_VALID.value):
                Q_d.append(dut.QUOTIENT.value.signed_integer)
            yield RisingEdge(dut.CLK)
        dut.INPUT_VALID <= 0
        for _ in range(interruption):
            if (dut.OUTPUT_VALID.value):
                Q_d.append(dut.QUOTIENT.value.signed_integer)
            yield RisingEdge(dut.CLK)
    while(len(Q_d) < len(Q_e)):
        if(dut.OUTPUT_VALID.value):
            Q_d.append(dut.QUOTIENT.value.signed_integer)
        yield RisingEdge(dut.CLK)
    if  Q_e != Q_d:
        raise TestFailure("Not match: needed {}\nwas {}".format(Q_e, Q_d))