###############################################################################
#
# This file is part of the lut_brightness_transform project
# (https://gitlab.com/dcaruso/lut-brightness-transform).
# Copyright (c) 2019 David M. Caruso.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
#
# Description:
#
# Python class for the LUT brightness transform core
#
###############################################################################

import cocotb
import random
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue
from random import randint

CLK_PERIOD = 1

class LutBrightnessTransform:
    def __init__(self, dut):
        self.dut = dut
        self.dut._log.info('Init Device Under Test (DUT)')
        self.dut._log.info('Init Clock')
        cocotb.fork(Clock(self.dut.CLK, CLK_PERIOD, units='ns').start())
        self.dut.DATA_STREAM_INPUT <= 0
        self.dut.DATA_IN_VALID <= 0
        self.dut.DATA_LUT_IN <= 0
        self.dut.DATA_LUT_IN_VALID <= 0
        self.dut.ADDR_LUT_IN <= 0
        self.dut.DATA_LUT_OUT <= 0
        self.dut.DATA_LUT_OUT_VALID <= 0
        self.dut.ADDR_LUT_OUT <= 0
        self.dut.LOAD_TABLE <= 0
        self.READ_TABLE = 1
        self.LOAD_TABLE = 0
        self.LUT_LEN = self.dut.LUT_LEN.value.integer
        self.WIDTH_IN = self.dut.STREAM_INPUT_WIDTH.value.integer
        self.WIDTH_OUT = self.dut.STREAM_OUTPUT_WIDTH.value.integer
        self.lut_in = [[0] * self.LUT_LEN, [0] * self.LUT_LEN]
        self.lut_out = [[0] * self.LUT_LEN, [0] * self.LUT_LEN]
        self.DATA_OUT = []
        self.XA_d = []
        self.XB_d = []
        self.YA_d = []
        self.YB_d = []
        self.XI_d = []
        self.ADDRA_d = []
        self.ADDRB_d = []

    @cocotb.coroutine
    def reset(self):
        self.dut._log.info('Reset DUT')
        self.dut.nRST <= 0
        for i in range(10):
            yield RisingEdge(self.dut.CLK)
        self.dut.nRST <= 1

    @cocotb.coroutine
    def fill_input_lut(self, value, addr):
        self.dut.DATA_LUT_IN <= value
        self.lut_in[self.LOAD_TABLE][addr] = value
        self.dut.DATA_LUT_IN_VALID <= 1
        self.dut.ADDR_LUT_IN <= addr
        yield RisingEdge(self.dut.CLK)
        self.dut.DATA_LUT_IN_VALID <= 0

    @cocotb.coroutine
    def fill_output_lut(self, value, addr):
        self.dut.DATA_LUT_OUT <= value
        self.lut_out[self.LOAD_TABLE][addr] = value
        self.dut.DATA_LUT_OUT_VALID <= 1
        self.dut.ADDR_LUT_OUT <= addr
        yield RisingEdge(self.dut.CLK)
        self.dut.DATA_LUT_OUT_VALID <= 0

    @cocotb.coroutine
    def new_input(self, value):
        self.dut.DATA_STREAM_INPUT <= value
        self.dut.DATA_IN_VALID <= 1
        yield RisingEdge(self.dut.CLK)
        self.dut.DATA_IN_VALID <= 0

    def get_lut_out_value(self, addr):
        if addr >= self.LUT_LEN:
            return self.lut_out[self.READ_TABLE][self.LUT_LEN-1]
        if addr < 0:
            return self.lut_out[self.READ_TABLE][0]
        return self.lut_out[self.READ_TABLE][addr]

    def get_lut_in_value(self, addr):
        if addr >= self.LUT_LEN:
            return self.lut_in[self.READ_TABLE][self.LUT_LEN-1]
        if addr < 0:
            return self.lut_in[self.READ_TABLE][0]
        return self.lut_in[self.READ_TABLE][addr]

    @cocotb.coroutine
    def select_work_table(self, table):
        self.dut.LOAD_TABLE <= 1-table
        self.LOAD_TABLE = 1-table
        self.READ_TABLE = table
        yield RisingEdge(self.dut.CLK)

    @cocotb.coroutine
    def create_new_function_lut(self, table):
        yield self.select_work_table(1-table)
        value_in = random.randint(-(2**(self.WIDTH_IN-1)), (2**(self.WIDTH_IN-1)-1))
        value_out = random.randint(0, (2**(self.WIDTH_OUT))-1)
        for i in range(self.LUT_LEN):
            yield self.fill_input_lut(value_in, i)
            yield self.fill_output_lut(value_out, i)
            if value_in < (2**(self.WIDTH_IN-1)-1):
                value_in = random.randint(value_in+1, (2**(self.WIDTH_IN-1)-1))
            value_out = random.randint(0, (2**(self.WIDTH_OUT))-1)

    @cocotb.coroutine
    def output_collector(self):
        self.dut._log.info('Starting output collector')
        while(True):
            if (self.dut.DATA_OUT_VALID.value):
                self.DATA_OUT.append(self.dut.DATA_STREAM_OUTPUT.value.integer)
            yield RisingEdge(self.dut.CLK)

    def get_output_collected(self):
        return self.DATA_OUT

    def get_used_function(self):
        return self.lut_in[self.READ_TABLE], self.lut_out[self.READ_TABLE]
